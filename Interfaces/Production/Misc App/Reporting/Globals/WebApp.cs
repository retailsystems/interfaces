using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

//enum me { a, b };

namespace Globals
{
    enum ConfigItems
    {    
       SmtpServer,
       ReportServiceURL,
       DBConnection,
       DBConnection2
     };

    class WebApp
    {
        public string this[ConfigItems itemName]
        {
              get
              {
                  switch (itemName)
                  {
                        default:
                        return AppSettings.Item[itemName.ToString];
                  }     
              }            
        }
    }
}

