using System;
//using System.Web.Mail;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;
using System.Web.Services.Protocols;
using ReportServiceUtil.casql05dev;
using HotTopic.AIMS.Logging;
using System.Collections;

namespace ReportServiceUtil
{
	/// <summary>
	/// Author: Tuong Nguyen
	/// Date: 02/05/2007
	/// Desc: Template reporting services
	/// Revision History:
	/// </summary>
	/// 
	class Process
	{
		#region public variable
        private string DBConnection;

			public static string reportPath = String.Empty;
		    public static string reportID = String.Empty;	
        
            //public static string url = String.Empty;
            public static string smtpServer = String.Empty;
            public static string delivery = String.Empty;
            public static int reportTimeout;   

            public string webserviceID = String.Empty;
            public string locationID = String.Empty;
            public static string format = String.Empty;

            public static byte[] result = null;
            public static string fileName = String.Empty;
            public static string reportName = String.Empty;
            public string status = String.Empty;    
            public string modified = String.Empty;
            public string emailFrom = String.Empty;
            public string emailType = String.Empty;
            public string emailAddress = String.Empty;
            public string emailModified = String.Empty;
            public string emailTo = String.Empty;
            public string emailCC = String.Empty;
            public string emailBCC = String.Empty;
            public static string outputLocn = String.Empty;
            public string webserivceURL = String.Empty;
            public ParameterValue[] parameters;
			
            public string emailBody = String.Empty;
			public string emailSubject = String.Empty;

			public const string EXCEL_FORMAT = "EXCEL";
			public const string EXCEL_FORMAT_EXT = ".xls";

			public const string PDF_FORMAT = "PDF";
			public const string PDF_FORMAT_EXT = ".pdf";

			public const string XML_FORMAT = "XML";
			public const string XML_FORMAT_EXT = ".xml";

			public const string TIFF_FORMAT = "TIFF";
			public const string TIFF_FORMAT_EXT = ".tif";

			public const string ERROR = "ERROR: ";
			public static FileLogger fileLoger;
			public static string sLogFile = String.Empty;

		#endregion
		/*----------------------------------------------------------------------------------------------*/
		/* Process starts at Main																	*/
		/*----------------------------------------------------------------------------------------------*/
        static void Main(string[] args)
        {
            try
            {
                Process process = new Process();
                process.InitializeApp();

                StringBuilder logMessages = new StringBuilder();
                fileLoger = new FileLogger(sLogFile);
                fileLoger.WriteItem("===== Start process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);

                if( args.Length <= 0 )
                   {
                       Console.WriteLine("Please specify a report to generate!");
                       fileLoger.WriteItem("No report speicified to generate");
                       fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
                       System.Environment.Exit(1);
                   }

                string reportName = args[0].Trim();
                //For debugging
                //string reportName = "Allocation Productivity Rpt2";
            
                process.LoadReportConfig(reportName);
                process.GetParameters(reportID);

                string fileName = outputLocn + reportName + "_" + DateTime.Now.ToString("MM_dd_yyyy").Replace("/", "").Replace(":", "").Replace(" ", "") + process.GetFileExtension(format);
                process.RenderReport();
                
                // Write the contents of the report to file.
                process.WriteToFile(fileName, result);
                fileLoger.WriteItem("Report " + fileName + " is generated.");

                if (delivery == "EMAIL")
                {
                    process.SendEmail(reportID, fileName);
                    fileLoger.WriteItem("Report " + fileName + " is emailed.");
                }

                fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);

                Console.WriteLine("0 - Report " + reportName + " successful.");
                
            }
            catch (SoapException ex)
            {
                Console.WriteLine(ex.Message);
                //Console.ReadLine();
                fileLoger.WriteItem(ERROR + fileName + " - " + ex.Message);
                fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
                Environment.Exit(1);
            } 
            catch (Exception ex)
            {
                Console.WriteLine("1 - Error: " + ex.Message);
                fileLoger.WriteItem(ERROR + ex.Message);
                fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
                Environment.Exit(1);
            }
        }
		/*----------------------------------------------------------------------------------------------*/
		/* Initialize global variables																	*/
		/*----------------------------------------------------------------------------------------------*/
		private void InitializeApp()
		{
			String value = String.Empty;
			AppSettingsReader settings = new AppSettingsReader();
            if(!Directory.Exists((string)settings.GetValue("LogFileLocation", value.GetType())))
            {
                Directory.CreateDirectory((string)settings.GetValue("LogFileLocation", value.GetType()));
            }
            sLogFile = (string)settings.GetValue("LogFileLocation", value.GetType()) + "\\" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".log";
			DBConnection = (string)settings.GetValue("DBConnection",value.GetType());
            smtpServer = (string)settings.GetValue("SmtpServer", value.GetType());
            reportTimeout = Convert.ToInt32(settings.GetValue("ReportTimeout", value.GetType()));
		}
        
        /*----------------------------------------------------------------------------------------------*/
		/* Load report configuration database															*/
		/*----------------------------------------------------------------------------------------------*/
		private void LoadReportConfig(string reportName)
		{
			
			SqlConnection dbCon = new SqlConnection(DBConnection);
			SqlDataReader rdr = null;
			StringBuilder sql = new StringBuilder();
			sql.Append("select a.*, b.output_location, c.webservice_URL from report_config a, output_location b, webservices c where a.report_name = '");
			sql.Append(reportName);
            sql.Append("' and a.location_id = b.location_id and a.webservice_id = c.webservice_id");

            try
            {
                dbCon.Open();
                SqlCommand cmd = new SqlCommand(sql.ToString(), dbCon);
                rdr = cmd.ExecuteReader();

                // Check if we have a row for our report
                if (!rdr.HasRows)
                {
                    //MessageBox.Show("Test");
                    throw new Exception("NoRows");
                }

                if (rdr.Read())
                {
                    //reportPath = rdr["LOCATION"].ToString();
                    reportID = rdr["Report_ID"].ToString();
                    reportPath = rdr["Report_path"].ToString();
                    format = rdr["Format"].ToString();
                    delivery = rdr["Delivery_method"].ToString();
                    emailSubject = rdr["Email_subject"].ToString();
                    emailSubject = emailSubject + " for " + DateTime.Now.ToString("MM/dd/yyyy");
                    emailBody = rdr["Email_body"].ToString();
                    emailFrom = rdr["Email_from"].ToString();
                    modified = rdr["MOD_DATE_TIME"].ToString();
                    outputLocn = rdr["Output_location"].ToString();
                    webserivceURL = rdr["Webservice_URL"].ToString();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "NoRows")
                {
                    Console.WriteLine("1 - Error: Invalid report name specified. Report Failed! " + ex.Message);
                    fileLoger.WriteItem(ERROR + "Invalid report name specified. Report Failed! " + ex.Message);
                    fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
                }
                else
                {
                    Console.WriteLine("1 - Error: Error opening database: " + ex.Message);
                    fileLoger.WriteItem(ERROR + "There is an error trying open database: " + ex.Message);
                    fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
                }
            }
        
               finally
			{
				if( rdr != null || dbCon != null )
				{
					rdr.Close();
					dbCon.Close();
				}
		    }
        }
        /*----------------------------------------------------------------------------------------------*/
		/* Get report parameters																*/
		/*----------------------------------------------------------------------------------------------*/
        private void GetParameters(string reportID)
        {
            SqlConnection dbCon = new SqlConnection(DBConnection);
            SqlDataReader rdr = null;
            StringBuilder sql = new StringBuilder();
            StringBuilder sqlCt = new StringBuilder();
            sql.Append("select * from parameters where report_id = '");
            sql.Append(reportID);
            sql.Append("'");
            sqlCt.Append("select count(*) from parameters where report_id = '");
            sqlCt.Append(reportID);
            sqlCt.Append("'");

            try
            {
                dbCon.Open();
                SqlCommand cmdCt = new SqlCommand(sqlCt.ToString(), dbCon);
                Int32 count = (Int32)cmdCt.ExecuteScalar();
                parameters = new ParameterValue[count];
                SqlCommand cmd = new SqlCommand(sql.ToString(), dbCon);
                rdr = cmd.ExecuteReader();
                int i = 0;

                while( rdr.Read())
                {
                        parameters[i] = new ParameterValue();
                        parameters[i].Name = rdr["Parameter_name"].ToString();
                        parameters[i].Value = rdr["Parameter_value"].ToString();
                        i++;
                }
            }

            catch (Exception ex)
            {
                    Console.WriteLine("1 - Error: Error in getting parameters: " + ex.Message);
                    fileLoger.WriteItem(ERROR + "Error in getting parameters: " + ex.Message);
                    fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
            }
            finally
            {
                if (rdr != null || dbCon != null)
                {
                    rdr.Close();
                    dbCon.Close();
                }
            }
        }

        /*----------------------------------------------------------------------------------------------*/
        /* Send Report															*/
        /*----------------------------------------------------------------------------------------------*/
        private void SendEmail(string reportID, string fileAttached)
        {
            SqlConnection dbCon = new SqlConnection(DBConnection);
            SqlDataReader rdr = null;
            StringBuilder sql = new StringBuilder();
            sql.Append("select * from email_master where report_ID = '");
            sql.Append(reportID);
            sql.Append("';");
            EmailManager eManager = new EmailManager(smtpServer);
            eManager.Sender = new MailAddress(emailFrom);

            try
            {
                dbCon.Open();

                SqlCommand cmd = new SqlCommand(sql.ToString(), dbCon);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if (rdr["Email_type"].Equals("TO"))
                    {
                        emailTo = rdr["Email_address"].ToString();
                        eManager.AddTo(emailTo);
                    }
                    else if (rdr["Email_type"].Equals("CC"))
                    {
                        emailCC = rdr["Email_address"].ToString();
                        eManager.AddCC(emailCC);
                    }
                    else if (rdr["Email_type"].Equals("BCC"))
                    {
                        emailBCC = rdr["Email_address"].ToString();
                        eManager.AddBCC(emailBCC);
                    }
                }
                    
                eManager.AddAttachment(fileAttached);
                eManager.Subject = emailSubject;
                eManager.Body = emailBody;
                eManager.Priority = MailPriority.Normal;
                eManager.Send();

                /* This block is for .NET Framework < 2.0
                MailMessage mail = new MailMessage(); 
                mail.To = 
                mail.To = receivepient; 
                mail.From = sender; 
                mail.Subject = emailSubject; 
                mail.Body = emailBody; 
                mail.Attachments.Add( new MailAttachment(fileAttached));
                SmtpMail.SmtpServer = "filter"; 
                SmtpMail.Send(mail); 
                Console.WriteLine("Sending email to: " + receivepient);
                */
           }
           catch (Exception ex)
           {
                Console.WriteLine("1 - Error in sending report: " + ex.Message);
                fileLoger.WriteItem(ERROR + "There is an error in sending the report: " + ex.Message);
                fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
           }
           finally
           {
                if (rdr != null || dbCon != null)
                {
                    rdr.Close();
                    dbCon.Close();
                }
           }
        }
        /*----------------------------------------------------------------------------------------------*/
		/* Get file extension 															*/
		/*----------------------------------------------------------------------------------------------*/
		private string GetFileExtension(string fileFormat)
		{
			string fileExtension = String.Empty;
			switch( fileFormat)
			{
				case EXCEL_FORMAT:
					fileExtension = EXCEL_FORMAT_EXT;
					break;
				case PDF_FORMAT:
					fileExtension = PDF_FORMAT_EXT;
					break;
				case XML_FORMAT:
					fileExtension = XML_FORMAT_EXT;
					break;
				default:
					break;
			}
			return fileExtension;
		}
		/*----------------------------------------------------------------------------------------------*/
		/* Write report to local file system															*/
		/*----------------------------------------------------------------------------------------------*/
		private void WriteToFile(string fileName,byte[] content)
		{
        	try
			{
				FileStream stream = File.Create(fileName, content.Length);
                fileLoger.WriteItem("File created.");

				stream.Write(content, 0, content.Length);
                fileLoger.WriteItem("Result written to the file.");
				stream.Close();
			}
			catch (Exception ex)
			{
                Console.WriteLine("1 - Error opening report: " + ex.Message);
				fileLoger.WriteItem(ERROR + " There is an error open the report -  " + fileName + " - " + ex.Message);
				fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
			}
		}
        /*----------------------------------------------------------------------------------------------*/
		/* Render report															*/
		/*----------------------------------------------------------------------------------------------*/
        private void RenderReport()
        {
            //try
            //{
                ReportExecutionService rs = new ReportExecutionService();
                rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
                rs.Url = webserivceURL;
 
                // Render arguments
                //byte[] result = null;
                string historyID = null;
                string devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                //DataSourceCredentials[] credentials = null;
                //string showHideToggle = null;
                string encoding;
                string mimeType;
                string extension;
                Warning[] warnings = null;
                string[] streamIDs = null;

                ExecutionInfo execInfo = new ExecutionInfo();
                ExecutionHeader execHeader = new ExecutionHeader();

                rs.ExecutionHeaderValue = execHeader;
                execInfo = rs.LoadReport(reportPath, historyID);
            
                rs.SetExecutionParameters(parameters, "en-us"); 
                String SessionId = rs.ExecutionHeaderValue.ExecutionID;
                rs.Timeout = reportTimeout;

                result = rs.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);
                execInfo = rs.GetExecutionInfo();
            //}
            //catch (SoapException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    //Console.ReadLine();
            //    fileLoger.WriteItem(ERROR + fileName + " - " + ex.Message);
            //    fileLoger.WriteItem("==== End process at " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ===== \r\n", 1);
            //} 
        }
	}
}
