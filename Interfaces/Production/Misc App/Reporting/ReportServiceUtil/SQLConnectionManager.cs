using System;
using System.Data.SqlClient;

namespace ReportServiceUtil
{
	/// <summary>
	/// Summary description for SQLConnectionManager.
	/// </summary>
	public class SQLConnectionManager
	{
		private string _connectionString;
		private SqlConnection _sqlConn;

		public SQLConnectionManager(string connectionString)
		{
			_connectionString = connectionString;
		}

		public void Connect()
		{
			if (_sqlConn == null)
			{
				_sqlConn = new SqlConnection(_connectionString);
			}
			if (_sqlConn.State == System.Data.ConnectionState.Closed)
			{
				_sqlConn.Open();
			}
		}
		public void Disconnect()
		{
			if (_sqlConn != null)
			{
				try
				{
					_sqlConn.Close();
				}
				catch (Exception ex)
				{
					throw ex;
				}
				finally
				{
					_sqlConn.Dispose();
				}
			}
		}

		public string ConnectionString
		{
			get
			{
				return _connectionString;
			}
			set
			{
				_connectionString = value;
			}
		}

		public System.Data.DataSet ExecuteDataSet(string sqlStatement)
		{
			SqlCommand sqlCmd = new SqlCommand(sqlStatement, _sqlConn);
			System.Data.DataSet ds = new System.Data.DataSet();
			SqlDataAdapter dataAdapter = new SqlDataAdapter();

			try
			{

				dataAdapter.SelectCommand = sqlCmd;
				dataAdapter.Fill(ds);
				return ds;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (dataAdapter != null)
				{
					dataAdapter.Dispose();
				}
			}
		}
		
		public void ExecuteQuery(string sqlStatement)
		{
			SqlCommand sqlCmd = new SqlCommand(sqlStatement, _sqlConn);

			try
			{
				sqlCmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (sqlCmd != null)
				{
					sqlCmd.Dispose();
				}
			}
		}

	}
}
