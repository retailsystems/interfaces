<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Lookup.aspx.cs" Inherits="ReportServiceWebUI.Lookup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlLookup" runat="server" GroupingText=" " Height="64px" Width="728px">
            <table style="width: 100%; height: 100%">
                <tr>
                    <td style="width: 214px; height: 55px">
                    </td>
                    <td style="vertical-align: middle; width: 378px; height: 55px; text-align: center">
                        <strong>Report Execution Lookup</strong></td>
                    <td style="vertical-align: middle; width: 198px; height: 55px; text-align: center">
                        <asp:Label ID="lblDateTime" runat="server" Font-Bold="True" Text="Label" Width="117px"></asp:Label></td>
                </tr>
            </table>
        </asp:Panel>
    
    </div>
        <br />
        <asp:Panel ID="pnlRptLookupDtls" runat="server" GroupingText="Reports" Height="207px"
            Width="703px">
            <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                AutoGenerateSelectButton="True" BackColor="White" BorderColor="#999999" BorderStyle="None"
                BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="717px">
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="#DCDCDC" />
            </asp:GridView>
        </asp:Panel>
        &nbsp;<br />
        <asp:Button ID="btnOk" runat="server" Text="Ok" Width="114px" /><asp:Button ID="btnCancel"
            runat="server" Text="Cancel" Width="114px" />
    </form>
</body>
</html>
