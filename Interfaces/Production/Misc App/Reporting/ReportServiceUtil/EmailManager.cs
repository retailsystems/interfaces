using System;
using System.Net.Mail;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ReportServiceUtil
{
   //
   // Author: Tuong Nguyen
   // Date:   02/06/2007
   // Desc:   Email manager
   // Revision History:
   //
 
    class EmailManager
    {
        MailMessage mailMessage = null;
        private SmtpClient client;

        //public class MailAddressCollection : Collection<MailAddress>
        //public MailAddressCollection ()
        
        public EmailManager(string smtpServer)
        {
            mailMessage = new MailMessage();
            client = new SmtpClient(smtpServer);
            mailMessage.Priority = MailPriority.Normal;
        }
        public void AddTo(string emailTo)
        {
            mailMessage.To.Add(new MailAddress(emailTo));
        }
        public void AddCC(string emailCC)
        {
            mailMessage.CC.Add(new MailAddress(emailCC));
            //MailAddress XYZ = new MailAddress(emailCC);
            //mailMessage.CC.Add(XYZ);
        }
        public string Subject
        {
            get
            {
                return mailMessage.Subject;
            }
            set
            {
                mailMessage.Subject = value;
            }
        }
        public MailAddress Sender
        {
            get
            {
                return mailMessage.From;
            }
            set
            {
                mailMessage.From = value;
            }
        }

        public void AddBCC(string emailBCC)
        {
            mailMessage.Bcc.Add(new MailAddress(emailBCC));
        }
        public string Body
        {
            get
            {
                return mailMessage.Body ;
            }
            set
            {
                mailMessage.Body = value;
            }
        }
        public void AddAttachment(string attachment)
        {
            mailMessage.Attachments.Add(new Attachment(attachment));
        }
        public MailPriority Priority
        {
            get
            {
                return mailMessage.Priority;
            }
            set
            {
                mailMessage.Priority = value;
            }
        }
        public void Send()
        {
                client.Send(mailMessage);
        }
    }
}
