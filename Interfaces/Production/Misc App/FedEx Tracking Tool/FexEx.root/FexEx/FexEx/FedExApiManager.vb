Imports HotTopic.DCSS.Services
Imports HotTopic.DCSS.Settings
Imports System.Data.SqlClient
Imports HotTopic.DCSS.UpsApi
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml
Imports System.Web.Services.Protocols
Imports FedEx.WebReference
Imports System.Configuration



Public Class FedExApiManager
    Public debugStr As String
    Public shpMnt As Shipment
    Private _XmlReq As String
    Private _XmlRes As String
    Public HttpPostFailCnt As Int16 = 0


    Public TotCnt As Int16
    Public SuccessCnt As Int16
    Public ProcessBeginDt As Date
    Private PrcoessCnt As Int16

    'Private _trackDs As DataSet
    ' Returns all non-delivered Shipments from DB
    Public Function GetPendingShipments(ByVal brand, ByVal provider) As DataSet
        Try
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@Brand", SqlDbType.VarChar, 2)
            arParms(0).Value = brand
            arParms(1) = New SqlParameter("@Provider", SqlDbType.VarChar, 25)
            arParms(1).Value = provider
            'Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListUpsTrackingDs", arParms)
            Return SQLDataManager.GetInstance().GetDataSet("DCSP_ListUpsTrackingDs_ByBrandByProvider", arParms)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub UpdateShipmentInfo()
        Try
            Dim arParms() As SqlParameter = New SqlParameter(8) {}
            arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
            arParms(0).Value = shpMnt.TrackingNum
            arParms(1) = New SqlParameter("@StatusCD", SqlDbType.SmallInt)
            arParms(1).Value = shpMnt.Status
            arParms(2) = New SqlParameter("@Comments", SqlDbType.NVarChar, 2000)
            arParms(2).Value = ""
            arParms(3) = New SqlParameter("@UserID", SqlDbType.VarChar)
            arParms(3).Value = "System"
            arParms(4) = New SqlParameter("@ScheduledDeliveryDate", SqlDbType.DateTime)
            arParms(4).Value = UtilityManager.DateToNull(shpMnt.ScheduledDeliveryDate)
            arParms(5) = New SqlParameter("@DeliveredDate", SqlDbType.DateTime)
            arParms(5).Value = UtilityManager.DateToNull(shpMnt.DeliveredDate)
            arParms(6) = New SqlParameter("@UpdateFlag", SqlDbType.Bit)
            arParms(6).Value = 1
            arParms(7) = New SqlParameter("@UpsErrorCode", SqlDbType.Int)
            arParms(7).Value = UtilityManager.IntegerToNull(shpMnt.UpsErrorCode)
            arParms(8) = New SqlParameter("@UpsErrorDesc", SqlDbType.VarChar, 500)
            arParms(8).Value = UtilityManager.StringToNull(shpMnt.UpsErrorDesc)

            SQLDataManager.GetInstance().Execute("DCSP_UpdateShipmentInfo", arParms)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UpdateFedExShipmentInfo(ByVal Carrier_PickupFlag As String)
        Try
            Dim arParms() As SqlParameter = New SqlParameter(4) {}
            arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
            arParms(0).Value = shpMnt.TrackingNum
            arParms(1) = New SqlParameter("@StatusCD", SqlDbType.VarChar, 50)
            arParms(1).Value = shpMnt.FedExStatus
            arParms(2) = New SqlParameter("@StatusDesc", SqlDbType.VarChar, 255)
            arParms(2).Value = shpMnt.FedExStatusDesc
            arParms(3) = New SqlParameter("@Carrier_DeliveredDate", SqlDbType.DateTime)
            arParms(3).Value = UtilityManager.DateToNull(shpMnt.DeliveredDate)
            arParms(4) = New SqlParameter("@Carrier_PickupFlag", SqlDbType.VarChar, 1)
            arParms(4).Value = Carrier_PickupFlag

            SQLDataManager.GetInstance().Execute("DCSP_UpdateFedExShipmentStatus", arParms)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LogException(ByVal ErrorCode As ErrorCode, ByVal e As Exception)
        Try

            Dim arParms() As SqlParameter = New SqlParameter(2) {}
            arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
            arParms(0).Value = shpMnt.TrackingNum
            arParms(1) = New SqlParameter("@ErrorCode", SqlDbType.SmallInt)
            arParms(1).Value = ErrorCode
            arParms(2) = New SqlParameter("@ErrorMsg", SqlDbType.NVarChar, 2000)
            arParms(2).Value = Left(e.Message, 2000)
            SQLDataManager.GetInstance().Execute("DCSP_LogUpsApiError", arParms)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LogExceptionTest(ByVal ErrorCode As ErrorCode, ByVal e As String)
        Try

            Dim arParms() As SqlParameter = New SqlParameter(2) {}
            arParms(0) = New SqlParameter("@UpsTracking", SqlDbType.VarChar, 30)
            arParms(0).Value = shpMnt.TrackingNum
            arParms(1) = New SqlParameter("@ErrorCode", SqlDbType.SmallInt)
            arParms(1).Value = ErrorCode
            arParms(2) = New SqlParameter("@ErrorMsg", SqlDbType.NVarChar, 2000)
            arParms(2).Value = e
            SQLDataManager.GetInstance().Execute("DCSP_LogUpsApiError", arParms)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetDateFromFedExDate(ByVal dateStr As String, ByVal timeStr As String) As DateTime
        Dim tempDateTimeStr As String = ""
        tempDateTimeStr = dateStr.Substring(5, 2) & "/" & dateStr.Substring(8, 2) & "/" & dateStr.Substring(0, 4)
        If timeStr.Trim.Length = 8 Then
            tempDateTimeStr &= " " & timeStr
        Else
            tempDateTimeStr &= " 00:00:00"
        End If
        Return tempDateTimeStr
    End Function

    Public Sub SendMail(ByVal mailType As Int16, Optional ByVal failureReason As String = "")
        Try
            Dim emailMgr As EmailManager = New EmailManager(HotTopic.DCSS.Settings.AppSetting.SmtpServer)
            Dim mailBody, msg As String
            'mail type 1-sucess 2-exception
            Select Case mailType
                Case 1
                    mailBody = HotTopic.DCSS.Settings.AppSetting.SuccessEmailBody
                Case 2
                    mailBody = HotTopic.DCSS.Settings.AppSetting.CancellationEmailBody
            End Select

            mailBody = mailBody.Replace("%beginDate%", ProcessBeginDt)
            mailBody = mailBody.Replace("%endDate%", Now)
            mailBody = mailBody.Replace("%totCnt%", TotCnt)
            mailBody = mailBody.Replace("%processCnt%", PrcoessCnt)
            mailBody = mailBody.Replace("%failCnt%", PrcoessCnt - SuccessCnt)
            mailBody = mailBody.Replace("%exception%", failureReason)
            '3/30/06 - Update process log.
            msg = "PROCESS: Batch" & vbNewLine
            msg &= "Begin date: " & ProcessBeginDt & vbNewLine
            msg &= "End date: " & Now & vbNewLine
            msg &= "Total # of records found: " & TotCnt & vbNewLine
            msg &= "Records processed: " & PrcoessCnt & vbNewLine
            msg &= "Records failed: " & PrcoessCnt - SuccessCnt & vbNewLine
            msg &= "Failure reason: " & IIf(failureReason.Trim = "", "N/A", failureReason.Trim) & vbNewLine
            LogMessage(HotTopic.DCSS.Settings.AppSetting.ProcessLog, msg)

            With emailMgr
                .AddRecipient(HotTopic.DCSS.Settings.AppSetting.DcssMailReceivers)
                .Sender = HotTopic.DCSS.Settings.AppSetting.SmtpSender
                .Subject = "DCSS Xml API process alert"
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub LogMessage(ByVal logFile As String, ByVal msg As String)
        Try
            Dim objStreamWriter As StreamWriter
            'Open the file.
            objStreamWriter = New StreamWriter(logFile, True)
            objStreamWriter.WriteLine("Log Entry Time: " & Now)
            objStreamWriter.WriteLine(msg)
            'Close the file.
            objStreamWriter.Close()
        Catch ex As Exception
            'Throw ex
        End Try
    End Sub

    Public Function GetFedExTrackingDetailsViaWebServices()
        Dim request As TrackRequest = CreateTrackRequest()
        Dim service As TrackService = New TrackService   ' Initialize the service

        'Internal values
        Dim _StatusCode As String = ""
        Dim _StatusDesc As String = ""
        Dim _tempDateStr As String = ""
        Dim _tempTimeStr As String = " 00:00:00"
        Dim _schdDeliveryDate As String = ""

        Dim _TestTrackingNum As String

        Dim Carrier_PickupFlag As String = ""

        Try
            ' Call the web service passing in a TrackRequest and returning a TrackReply
            Dim reply As TrackReply = service.track(request)

            PrcoessCnt += 1

            If (Not reply.TrackDetails Is Nothing) Then
                shpMnt.UpsErrorCode = 0
                shpMnt.UpsErrorDesc = ""

                For Each trackDetail As TrackDetail In reply.TrackDetails
                    _StatusCode = trackDetail.StatusCode
                    _StatusDesc = trackDetail.StatusDescription

                    _TestTrackingNum = trackDetail.TrackingNumber

                    shpMnt.FedExStatus = _StatusCode
                    shpMnt.FedExStatusDesc = _StatusDesc

                    If _StatusCode = "DL" Then
                        shpMnt.DeliveredDate = trackDetail.ActualDeliveryTimestamp
                    End If
                    Carrier_PickupFlag = "N"
                    If (trackDetail.EstimatedDeliveryTimestamp.ToString().Trim().Length > 0) Then
                        Carrier_PickupFlag = "Y"
                    End If

                    UpdateFedExShipmentInfo(Carrier_PickupFlag)

                    If _StatusCode = "DL" Then
                        SuccessCnt += 1
                    Else
                        _tempDateStr = trackDetail.EstimatedDeliveryTimestamp
                        _schdDeliveryDate = _tempDateStr '+ _tempTimeStr

                        If Not IsDBNull(UtilityManager.DateToNull(CDate(_schdDeliveryDate))) AndAlso (_schdDeliveryDate <> CStr(shpMnt.ScheduledDeliveryDate)) Then
                            shpMnt.ScheduledDeliveryDate = CDate(_schdDeliveryDate)
                            shpMnt.UpdateFlag = True
                        End If

                        If shpMnt.UpdateFlag Then
                            UpdateShipmentInfo()
                        End If

                        SuccessCnt += 1
                    End If
                Next
            End If

        Catch ex As FedExXmlException
            If ex.Message = "Missing FedEx XML Response" Then
                LogException(ErrorCode.HTTPPostFail, ex)
            Else
                LogException(ErrorCode.XmlParseError, ex)
            End If
        Catch ex As SqlException
            LogException(ErrorCode.DBUpdate, ex)
        Catch ex As Exception
            Throw ex
        End Try

    End Function



    Function CreateTrackRequest() As TrackRequest
        'Build the TrackRequest
        Dim request As TrackRequest = New TrackRequest
        '
        request.WebAuthenticationDetail = New WebAuthenticationDetail
        request.WebAuthenticationDetail.UserCredential = New WebAuthenticationCredential
        request.WebAuthenticationDetail.UserCredential.Key = ConfigurationManager.AppSettings("UserCredential_Key")
        request.WebAuthenticationDetail.UserCredential.Password = ConfigurationManager.AppSettings("UserCredential_Password")
        '
        request.ClientDetail = New ClientDetail
        request.ClientDetail.AccountNumber = ConfigurationManager.AppSettings("AccountNumber")
        request.ClientDetail.MeterNumber = ConfigurationManager.AppSettings("MeterNumber")
        '
        request.TransactionDetail = New TransactionDetail
        request.TransactionDetail.CustomerTransactionId = "*** ***" 'The client will get the same value back in the response
        '
        request.Version = New VersionId
        '
        request.PackageIdentifier = New TrackPackageIdentifier
        request.PackageIdentifier.Value = shpMnt.TrackingNum
        request.PackageIdentifier.Type = TrackIdentifierType.TRACKING_NUMBER_OR_DOORTAG
        request.ShipDateRangeBeginSpecified = False
        request.ShipDateRangeEndSpecified = False
        '
        request.IncludeDetailedScans = True ' Optional Use if all scans should be returned
        request.IncludeDetailedScansSpecified = True
        '
        Return request
    End Function

End Class
