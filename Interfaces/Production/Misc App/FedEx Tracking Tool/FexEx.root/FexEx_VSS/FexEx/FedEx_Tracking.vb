
Imports System.Xml
Imports System.Web.Services.Protocols
Imports FedEx.WebReference
Imports System.Configuration
Imports HotTopic.DCSS.Services
Imports HotTopic.DCSS.Settings
Imports HotTopic.DCSS.UpsAPI
Imports System.Threading
Imports FedEx.FedExApiManager

Module FedEx_Tracking

    Public settings As New HotTopic.DCSS.Settings.AppSetting
    Private _shipDs As DataSet
    Private _apiMgr As New FedExApiManager
    Private _dr As DataRow
    Private _shipment As Shipment
    Private _debugMode As Boolean = False
    Private _debugStr As String

    Private Sub InitializeAppSettings()

        'Get the configuration setting from the config file.
        With settings
            .ConnectionString = ConfigurationManager.AppSettings("ConnectionString")
            .SmtpServer = ConfigurationManager.AppSettings("SmtpServer")
            .SmtpSender = ConfigurationManager.AppSettings("SmtpSender")
            .HttpPostFailLimit = Convert.ToInt16(ConfigurationManager.AppSettings("HttpPostFailLimit"))
            .DcssMailReceivers = ConfigurationManager.AppSettings("DcssMailReceivers")
            .CancellationEmailBody = ConfigurationManager.AppSettings("CancellationEmailBody")
            .SuccessEmailBody = ConfigurationManager.AppSettings("SuccessEmailBody")
            .ProcessLog = ConfigurationManager.AppSettings("ProcessLog")
            If ConfigurationManager.AppSettings("DebugMode") = "TRUE" Then
                _debugMode = True
            End If
        End With

    End Sub


    Sub Main()
        ' get shipment dataset       
        Dim sleeptTimer As Int16

        InitializeAppSettings()

        Try
            If (Not IsNothing(ConfigurationManager.AppSettings("SleepTimer"))) AndAlso IsNumeric(ConfigurationManager.AppSettings("SleepTimer")) Then
                sleeptTimer = Convert.ToInt16(ConfigurationManager.AppSettings("SleepTimer"))
            End If

            Console.WriteLine("Begin Process:" & Now)
            Dim i As Integer = 0
            _shipDs = _apiMgr.GetPendingShipments
            _apiMgr.ProcessBeginDt = Now

            If _shipDs.Tables.Count > 0 Then
                _apiMgr.TotCnt = Convert.ToInt16(_shipDs.Tables(0).Rows.Count)
                Console.WriteLine(_apiMgr.TotCnt & " records found")
                For Each _dr In _shipDs.Tables(0).Rows

                    _debugStr = ""
                    _shipment = New Shipment
                    With _shipment
                        .TrackingNum = _dr("UpsTracking")
                        .StoreNum = _dr("StoreNum")
                        .ScheduledDeliveryDate = _dr("ScheduledDeliveryDate")
                        .Status = _dr("StatusCD")
                        .MeterNumber = _dr("MeterNumber")
                        .UpdateFlag = False
                    End With
                    _apiMgr.shpMnt = _shipment

                    _apiMgr.GetFedExTrackingDetailsViaWebServices()

                    'Build the debug string
                    _debugStr &= _apiMgr.shpMnt.TrackingNum & vbTab
                    _debugStr &= _apiMgr.shpMnt.Status & vbTab
                    _debugStr &= _apiMgr.shpMnt.ScheduledDeliveryDate & vbTab
                    _debugStr &= _apiMgr.shpMnt.DeliveredDate

                    'Display the tracking numbers in the comment prompt if the debug configuraiton is on.
                    If _debugMode Then
                        Console.WriteLine(_debugStr)
                    End If

                    'Thread.CurrentThread.Sleep(1000)
                    If sleeptTimer > 0 Then
                        Thread.CurrentThread.Sleep(sleeptTimer)
                    End If
                    i += 1
                    If i Mod 100 = 0 Then
                        Console.WriteLine(i & " records processed, time elapsed: " & DateDiff(DateInterval.Second, _apiMgr.ProcessBeginDt, Now) & " sec.")
                        'Exit For
                    End If

                Next
                _apiMgr.SendMail(1)
            End If

        Catch ex As Exception
            _apiMgr.SendMail(2, ex.Message)
            Console.WriteLine(ex.Message)
        Finally
            Console.WriteLine("End Process:" & Now & ", Total time elapsed:" & DateDiff(DateInterval.Second, _apiMgr.ProcessBeginDt, Now) & " sec.")
        End Try

    End Sub

End Module
