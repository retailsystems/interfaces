using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.OracleClient;
using HotTopic.AIMS.Logging;
using HotTopic.DCSS.Services;
using System.Configuration;
using HottopicUtilities;
using System.Transactions;

namespace AADiscrepancy
{
    class DiscrepancyManager
    {
        private String CADC_ConnStr = ConfigurationManager.AppSettings["CADC_ConnStr"];
        private String TNDC_ConnStr = ConfigurationManager.AppSettings["TNDC_ConnStr"];
        private String Arthur_ConnStr = ConfigurationManager.AppSettings["Arthur_ConnStr"];

        private LogUtil flLogFile = null;
        private DataSet dsItems = null;
        private DataSet dsBatch = null;
        private DataSet dsTNItems = null;
        private DataSet dsTNBatch = null;
        private DataSet dsSkus = null;
        private Int32 itemCount = 0;
        private Int32 itemTNCount = 0;
        private Int32 skuCount = 0;

        private string shpmt_NBR = String.Empty;
        private string distro_NBR = String.Empty;
        private string po_NBR = String.Empty;
        private string item_NBR = String.Empty;
        private string sku_NBR = String.Empty;
        
        public void Process()
        {
            flLogFile = Program.GetLogFile();

            //Get CA discrepancied items from WM
            dsItems = GetDiscrepanciedItems(CADC_ConnStr);
            itemCount = dsItems == null ? 0 : dsItems.Tables[0].Rows.Count;
            if (itemCount >= 1)
            {
                PopulateDiscrepancy(CADC_ConnStr, dsItems);
            }

            //Get TN discrepancied items from WM
            dsTNItems = GetDiscrepanciedItems(TNDC_ConnStr);
            itemTNCount = dsTNItems == null ? 0 : dsTNItems.Tables[0].Rows.Count;
            if (itemTNCount >= 1)
            {
                PopulateDiscrepancy(TNDC_ConnStr, dsTNItems);
            }
            
 
            //Get CA Discrepancy Batch
            dsBatch = GetDiscrepanciedItems_B(CADC_ConnStr);
            itemCount = dsBatch == null ? 0 : dsBatch.Tables[0].Rows.Count;
            if (itemCount >= 1)
            {
                PopulateDiscrepancy_Batch(CADC_ConnStr, dsBatch);
            }


            //Get TN Discrepancy Batch
            dsTNBatch = GetDiscrepanciedItems_B(TNDC_ConnStr);
            itemCount = dsTNBatch == null ? 0 : dsTNBatch.Tables[0].Rows.Count;
            if (itemCount >= 1)
            {
                PopulateDiscrepancy_Batch(TNDC_ConnStr, dsTNBatch);
            }


        }
        private void PopulateDiscrepancy(string connStr, DataSet dsItems)
        {
            

            Int32 oldWLKey = -1;
            Int32 newWLKey = -1;
            Int32 merchIDType = -1;
            Int16 i, j = 0;
            Int32 qtyRcv = 0;

            string strWLKeys = String.Empty;
            string[] arrWLKeys = null;

            for (i = 0; i < dsItems.Tables[0].Rows.Count; i++)
            {
                OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);
                dbCon.Open();
                OracleTransaction tran = dbCon.BeginTransaction();
                try
                {
                    shpmt_NBR = dsItems.Tables[0].Rows[i]["SHPMT_NBR"] == null ? String.Empty : dsItems.Tables[0].Rows[i]["SHPMT_NBR"].ToString();
                    distro_NBR = dsItems.Tables[0].Rows[i]["DISTRO_NBR"] == null ? String.Empty : dsItems.Tables[0].Rows[i]["DISTRO_NBR"].ToString();
                    po_NBR = dsItems.Tables[0].Rows[i]["PO_NBR"] == null ? String.Empty : dsItems.Tables[0].Rows[i]["PO_NBR"].ToString();
                    item_NBR = dsItems.Tables[0].Rows[i]["ITEM_NBR"] == null ? String.Empty : dsItems.Tables[0].Rows[i]["ITEM_NBR"].ToString();

                    if (shpmt_NBR != String.Empty && distro_NBR != String.Empty && po_NBR != String.Empty && item_NBR != String.Empty)
                    {
                        //using (dsSkus = GetDiscrepanciedSkus(shpmt_NBR, distro_NBR, po_NBR, item_NBR))
                        //{
                        //Get discrepancy skus from WM
                        flLogFile.WriteItem("=== Start query discrepancied sku number from WM");
                        dsSkus = GetDiscrepanciedSkus(shpmt_NBR, distro_NBR, po_NBR, item_NBR, connStr);
                        flLogFile.WriteItem("=== End query discrepancied sku number from WM.");
                        skuCount = dsSkus == null ? 0 : dsSkus.Tables[0].Rows.Count;
                        if (skuCount > 0)
                        {
                            if (!String.IsNullOrEmpty(shpmt_NBR) && !String.IsNullOrEmpty(distro_NBR) && !String.IsNullOrEmpty(po_NBR) && !String.IsNullOrEmpty(item_NBR))
                            {
                                //Populate to WorkList table --> returns worklist keys
                                string sku0 = dsSkus.Tables[0].Rows[0]["SKU_NBR"].ToString();
                                flLogFile.WriteItem("=== Start populate AA WorkList: ");
                                flLogFile.WriteItem("shpmtNBR = " + shpmt_NBR + ", distroNBR = " + distro_NBR + ", poNBR = " + po_NBR + ", itemNBR = " + item_NBR);
                                strWLKeys = PopulateAAWorkList(shpmt_NBR, distro_NBR, po_NBR, item_NBR, sku0, dbCon, tran);
                                flLogFile.WriteItem("=== End populate AA WorkList.");
                            }

                            arrWLKeys = strWLKeys.Split('|');

                            //oldWLKey = arrWLKeys[0].ToString() == null ? Convert.ToInt32(-1) : Convert.ToInt32(arrWLKeys[0]);
                            //newWLKey = arrWLKeys[1].ToString() == null ? Convert.ToInt32(-1) : Convert.ToInt32(arrWLKeys[1]);
                            //merchIDType = arrWLKeys[2].ToString() == null ? Convert.ToInt32(-1) : Convert.ToInt32(arrWLKeys[2]);

                            if (!String.IsNullOrEmpty(arrWLKeys[0].ToString()))
                                oldWLKey = Convert.ToInt32(arrWLKeys[0]);

                            if (!String.IsNullOrEmpty(arrWLKeys[1].ToString()))
                                newWLKey = Convert.ToInt32(arrWLKeys[1]);

                            //if (!String.IsNullOrEmpty(arrWLKeys[2].ToString()))
                            //    merchIDType = Convert.ToInt32(arrWLKeys[2]);


                            if (oldWLKey != -1 && newWLKey != -1 )
                            {
                                for (j = 0; j < skuCount; j++)
                                {
                                    qtyRcv = dsSkus.Tables[0].Rows[j]["UNITS_RCVD"] == null ? Convert.ToInt32(-1) : Convert.ToInt32(dsSkus.Tables[0].Rows[j]["UNITS_RCVD"]);
                                    sku_NBR = dsSkus.Tables[0].Rows[j]["SKU_NBR"] == null ? null : dsSkus.Tables[0].Rows[j]["SKU_NBR"].ToString();

                                    if (!String.IsNullOrEmpty(sku_NBR) && qtyRcv > -1)
                                    {
                                        //if (merchIDType == 1)
                                        //{
                                        //    flLogFile.WriteItem("=== Start Populate WorkList Bulk: ");
                                        //    flLogFile.WriteItem("oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", sku_NBR = " + sku_NBR + ", qtyRcv = " + qtyRcv);
                                        //    PopulateWorkListBulk(oldWLKey, newWLKey, sku_NBR, qtyRcv, dbCon, tran);
                                        //    flLogFile.WriteItem("=== End Populate WorkList Bulk.");
                                        //}
                                        //if (merchIDType == 2)
                                        //{
                                        //    flLogFile.WriteItem("=== Start Populate WorkList Multiple: ");
                                        //    flLogFile.WriteItem("oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", sku_NBR = " + sku_NBR + ", qtyRcv = " + qtyRcv);
                                        //    PopulateWorkListPack(oldWLKey, newWLKey, sku_NBR, qtyRcv, dbCon, tran);
                                        //    flLogFile.WriteItem("=== End Populate WorkList Bulk: ");
                                        //}

                                        flLogFile.WriteItem("=== Start Populate WorkList Pack: ");
                                        flLogFile.WriteItem("oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", sku_NBR = " + sku_NBR + ", qtyRcv = " + qtyRcv);
                                        PopulateWorkListPack1(oldWLKey, newWLKey, sku_NBR, qtyRcv, dbCon, tran);
                                        flLogFile.WriteItem("=== End Populate WorkList Pack: ");

                                        flLogFile.WriteItem("=== Start Recalculate WorkList: ");
                                        flLogFile.WriteItem("oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", sku_NBR = " + sku_NBR + ", qtyRcv = " + qtyRcv);
                                        RecalculateWorkList(newWLKey, merchIDType, dbCon, tran);
                                        flLogFile.WriteItem("=== End Recalculate WorkList.");
                                    }
                                }
                            }
                            else
                            {
                                string err = "ERROR: " + " oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", merchIDType = " + merchIDType;
                                flLogFile.WriteItem(err);
                                tran.Rollback();
                                UpdateDiscrepancyItems(shpmt_NBR, item_NBR, connStr, "E");
                                SendEmail("FAILED", err, 1);
                                continue;
                            }
                        }
                    }

                    // kjz 11-18-08 uncommented the below for testing
                    // ValidateWorkListAllocation(po_NBR, shpmt_NBR, distro_NBR, item_NBR, dbCon, tran);

                    //Commit transaction.
                    tran.Commit();
                    flLogFile.WriteItem("=== Transaction committed. ===");

                    //Update the processed status in temp table.
                    flLogFile.WriteItem("=== Start updating HT_ITEM_RECEIVE_TEMP: ");
                    UpdateDiscrepancyItems(shpmt_NBR, item_NBR, connStr, "Y");
                    flLogFile.WriteItem("=== End updating HT_ITEM_RECEIVE_TEMP. ");

                    //Send sucessful notify email
                    SendEmail("SUCCESS", "N/A", 1);
                   

                }
                catch (Exception ex)
                {
                    SendEmail("FAILED", ErrorHandler.GetErrorMessage(ex), 1);
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    tran.Rollback();
                    flLogFile.WriteItem("=== Transaction rollbacked. ===");
                }
                finally
                {
                    if (dbCon != null)
                    {
                        dbCon.Close();
                        dbCon.Dispose();
                    }
                }
            }//End For Loop
        }
        private void PopulateDiscrepancy_Batch(string connStr, DataSet dsBatch)
        {


            Int32 oldWLKey = -1;
            Int32 newWLKey = -1;
            Int32 merchIDType = -1;
            Int16 i, j = 0;
            Int32 qtyBack = 0;

            string strWLKeys = String.Empty;
            string[] arrWLKeys = null;

            for (i = 0; i < dsBatch.Tables[0].Rows.Count; i++)
            {
                OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);
                dbCon.Open();
                OracleTransaction tran = dbCon.BeginTransaction();
                try
                {
                   
                    distro_NBR = dsBatch.Tables[0].Rows[i]["DISTRO_NBR"] == null ? String.Empty : dsBatch.Tables[0].Rows[i]["DISTRO_NBR"].ToString();                   
                    item_NBR = dsBatch.Tables[0].Rows[i]["ITEM_NBR"] == null ? String.Empty : dsBatch.Tables[0].Rows[i]["ITEM_NBR"].ToString();
                 
                    if (distro_NBR != String.Empty && item_NBR != String.Empty)
                    {
                        
                        flLogFile.WriteItem("=== Start query discrepancied sku number from WM");
                        dsSkus = GetDiscrepanciedSkus_B(distro_NBR, item_NBR, connStr);
                        flLogFile.WriteItem("=== End query discrepancied sku number from WM.");
                        skuCount = dsSkus == null ? 0 : dsSkus.Tables[0].Rows.Count;
                        if (skuCount > 0)
                        {
                            if (!String.IsNullOrEmpty(distro_NBR) && !String.IsNullOrEmpty(item_NBR))
                            {
                                //Populate to WorkList table --> returns worklist keys
                                string sku0 = dsSkus.Tables[0].Rows[0]["SKU_NBR"].ToString();
                                flLogFile.WriteItem("=== Start populate AA WorkList: ");
                                flLogFile.WriteItem("distroNBR = " + distro_NBR + ", itemNBR = " + item_NBR);
                                strWLKeys = PopulateAAWorkList_B(distro_NBR, item_NBR, sku0, dbCon, tran);
                                flLogFile.WriteItem("=== End populate AA WorkList.");
                            }

                            arrWLKeys = strWLKeys.Split('|');

                            //oldWLKey = arrWLKeys[0].ToString() == null ? Convert.ToInt32(-1) : Convert.ToInt32(arrWLKeys[0]);
                            //newWLKey = arrWLKeys[1].ToString() == null ? Convert.ToInt32(-1) : Convert.ToInt32(arrWLKeys[1]);
                            //merchIDType = arrWLKeys[2].ToString() == null ? Convert.ToInt32(-1) : Convert.ToInt32(arrWLKeys[2]);

                            if (!String.IsNullOrEmpty(arrWLKeys[0].ToString()))
                                oldWLKey = Convert.ToInt32(arrWLKeys[0]);

                            if (!String.IsNullOrEmpty(arrWLKeys[1].ToString()))
                                newWLKey = Convert.ToInt32(arrWLKeys[1]);

                            //if (!String.IsNullOrEmpty(arrWLKeys[2].ToString()))
                            //    merchIDType = Convert.ToInt32(arrWLKeys[2]);


                            if (oldWLKey != -1 && newWLKey != -1 )
                            {
                                for (j = 0; j < skuCount; j++)
                                {
                                    qtyBack = dsSkus.Tables[0].Rows[j]["BACK_UNITS"] == null ? Convert.ToInt32(-1) : Convert.ToInt32(dsSkus.Tables[0].Rows[j]["BACK_UNITS"]);
                                    sku_NBR = dsSkus.Tables[0].Rows[j]["SKU_NBR"] == null ? null : dsSkus.Tables[0].Rows[j]["SKU_NBR"].ToString();

                                    if (!String.IsNullOrEmpty(sku_NBR) && qtyBack > -1)
                                    {
                                       
                                        flLogFile.WriteItem("=== Start Populate WorkList Pack: ");
                                        flLogFile.WriteItem("oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", sku_NBR = " + sku_NBR + ", qtyBack = " + qtyBack);
                                        PopulateWorkListPack1_B(oldWLKey, newWLKey, sku_NBR, qtyBack, dbCon, tran);
                                        flLogFile.WriteItem("=== End Populate WorkList Pack: ");

                                        flLogFile.WriteItem("=== Start Recalculate WorkList: ");
                                        flLogFile.WriteItem("oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", sku_NBR = " + sku_NBR + ", qtyBack = " + qtyBack);
                                        RecalculateWorkList(newWLKey, merchIDType, dbCon, tran);
                                        flLogFile.WriteItem("=== End Recalculate WorkList.");
                                    }
                                }
                            }
                            else
                            {
                                string err = "ERROR: " + " oldWLKey = " + oldWLKey + ", newWLKey = " + newWLKey + ", merchIDType = " + merchIDType;
                                flLogFile.WriteItem(err);
                                tran.Rollback();
                                UpdateDiscrepancyItems_B(distro_NBR, item_NBR, sku_NBR, connStr, "E");
                                SendEmail("FAILED", err, 2);
                                continue;
                            }
                        }
                    }

                    // kjz 11-18-08 uncommented the below for testing
                    // ValidateWorkListAllocation(po_NBR, shpmt_NBR, distro_NBR, item_NBR, dbCon, tran);

                    //Commit transaction.
                    tran.Commit();
                    flLogFile.WriteItem("=== Transaction committed. ===");

                    //Update the processed status in temp table.
                    flLogFile.WriteItem("=== Start updating HT_ITEM_RECEIVE_TEMP: ");
                    UpdateDiscrepancyItems_B(distro_NBR, item_NBR, sku_NBR, connStr, "Y");
                    flLogFile.WriteItem("=== End updating HT_ITEM_RECEIVE_TEMP. ");

                    //Send sucessful notify email
                    SendEmail("SUCCESS", "N/A", 2);


                }
                catch (Exception ex)
                {
                    SendEmail("FAILED", ErrorHandler.GetErrorMessage(ex), 2);
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    tran.Rollback();
                    flLogFile.WriteItem("=== Transaction rollbacked. ===");
                }
                finally
                {
                    if (dbCon != null)
                    {
                        dbCon.Close();
                        dbCon.Dispose();
                    }
                }
            }//End For Loop
        }

       

        public DataSet GetDiscrepanciedItems(string connStr)
        {
            DataSet ds = null;
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT DISTINCT SHPMT_NBR,DISTRO_NBR,PO_NBR,ITEM_NBR");
            sql.Append(" FROM HT_ITEM_RECEIVE_TEMP");
            sql.Append(" WHERE PROCESSED IS NULL OR LENGTH(PROCESSED) <= 0");
            try
            {
                OracleDataManager dbCon = new OracleDataManager(connStr);
                ds = dbCon.ExecuteDataSet(sql.ToString());
                Int32 cnt = ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                throw;
            }
            return ds;
        }

        public DataSet GetDiscrepanciedItems_B(string connStr)
        {
            DataSet ds = null;
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT DISTINCT DISTRO_NBR, ITEM_NBR");
            sql.Append(" FROM HT_ITEM_RECEIVE_TEMP_BATCH");
            sql.Append(" WHERE PROCESSED IS NULL OR LENGTH(PROCESSED) <= 0");
            try
            {
                OracleDataManager dbCon = new OracleDataManager(connStr);
                ds = dbCon.ExecuteDataSet(sql.ToString());
                Int32 cnt = ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                throw;
            }
            return ds;
        }


        private void UpdateDiscrepancyItems(string shpmt_NBR, string item_NBR, string connStr, string process)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE HT_ITEM_RECEIVE_TEMP");
            sql.Append(" SET PROCESSED = '");
            sql.Append(process + "'");
            sql.Append(" WHERE SHPMT_NBR = '");
            sql.Append(shpmt_NBR + "'");
            sql.Append(" AND ITEM_NBR = '");
            sql.Append(item_NBR + "'");
            //TODO change try catch block
            try
            {
                OracleDataManager dbCon = new OracleDataManager(connStr);
                dbCon.ExecuteNonQuery(sql.ToString());
            }
            catch (Exception ex)
            {
                flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                throw;
            }
        }
        private void UpdateDiscrepancyItems_B(string distro_NBR, string item_NBR, string sku_NBR, string connStr, string process)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE HT_ITEM_RECEIVE_TEMP_BATCH");
            sql.Append(" SET PROCESSED = '");
            sql.Append(process + "'");
            sql.Append(" WHERE DISTRO_NBR = '");
            sql.Append(distro_NBR + "'");
            sql.Append(" AND ITEM_NBR = '");
            sql.Append(item_NBR + "'");
           // sql.Append(" AND SKU_NBR = '");
           // sql.Append(sku_NBR + "'");
            //TODO change try catch block
            try
            {
                OracleDataManager dbCon = new OracleDataManager(connStr);
                dbCon.ExecuteNonQuery(sql.ToString());
            }
            catch (Exception ex)
            {
                flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                throw;
            }
        }
        public DataSet GetDiscrepanciedSkus(string shpmtNBR, string distroNBR, string poNBR, string itemNBR, string connStr)
        {
            DataSet ds = null;
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT SHPMT_NBR,DISTRO_NBR,PO_NBR,ITEM_NBR, SKU_NBR, UNITS_RCVD");
            sql.Append(" FROM HT_ITEM_RECEIVE_TEMP");
            sql.Append(" WHERE (PROCESSED IS NULL OR LENGTH(PROCESSED) <= 0)");
            sql.Append(" AND SHPMT_NBR = ");
            sql.Append("'" + shpmtNBR + "'");
            sql.Append(" AND DISTRO_NBR = ");
            sql.Append("'" + distroNBR + "'");
            sql.Append(" AND PO_NBR = ");
            sql.Append("'" + poNBR + "'");
            sql.Append(" AND ITEM_NBR = ");
            sql.Append("'" + itemNBR + "'");
            try
            {
                OracleDataManager dbCon = new OracleDataManager(connStr);
                ds = dbCon.ExecuteDataSet(sql.ToString());
                Int32 cnt = ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                throw;
            }
            return ds;
        }

        public DataSet GetDiscrepanciedSkus_B(string distroNBR, string itemNBR, string connStr)
        {
            DataSet ds = null;
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT DISTRO_NBR, ITEM_NBR, SKU_NBR, BACK_UNITS");
            sql.Append(" FROM HT_ITEM_RECEIVE_TEMP_BATCH");
            sql.Append(" WHERE (PROCESSED IS NULL OR LENGTH(PROCESSED) <= 0)");          
            sql.Append(" AND DISTRO_NBR = ");
            sql.Append("'" + distroNBR + "'");
            sql.Append(" AND ITEM_NBR = ");
            sql.Append("'" + itemNBR + "'");
            try
            {
                OracleDataManager dbCon = new OracleDataManager(connStr);
                ds = dbCon.ExecuteDataSet(sql.ToString());
                Int32 cnt = ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                throw;
            }
            return ds;
        }


        private void ValidateWorkListAllocation(string poNBR, string shpmtNBR, string distroNBR, string itemNBR,OracleConnection dbCon,OracleTransaction tran)
        {
            if (!String.IsNullOrEmpty(shpmtNBR) && !String.IsNullOrEmpty(distroNBR) && !String.IsNullOrEmpty(poNBR) && !String.IsNullOrEmpty(itemNBR))
            {
                OracleParameter shpmtNBRParm = new OracleParameter("p_ASN_NBR", OracleType.VarChar);
                shpmtNBRParm.Direction = ParameterDirection.Input;
                shpmtNBRParm.SourceVersion = DataRowVersion.Current;
                shpmtNBRParm.Value = shpmtNBR;

                OracleParameter distroNBRParm = new OracleParameter("p_ALLOC_NBR", OracleType.VarChar);
                distroNBRParm.Direction = ParameterDirection.Input;
                distroNBRParm.SourceVersion = DataRowVersion.Current;
                distroNBRParm.Value = distroNBR;

                OracleParameter poNBRParm = new OracleParameter("p_PO_NBR", OracleType.VarChar);
                poNBRParm.Direction = ParameterDirection.Input;
                poNBRParm.SourceVersion = DataRowVersion.Current;
                poNBRParm.Value = poNBR;

                OracleParameter itemNBRParm = new OracleParameter("p_ITM_NBR", OracleType.VarChar);
                itemNBRParm.Direction = ParameterDirection.Input;
                itemNBRParm.SourceVersion = DataRowVersion.Current;
                itemNBRParm.Value = itemNBR;

                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Output;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Output;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;

                OracleParameter merchIDTypeParm = new OracleParameter("p_MERCH_ID_TYPE", OracleType.Number);
                merchIDTypeParm.Direction = ParameterDirection.Output;
                merchIDTypeParm.SourceVersion = DataRowVersion.Current;

                //OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);
                OracleCommand cmd = new OracleCommand("HT_RECEIVE.VALIDATE_WORKLIST_ALLOCATION", dbCon,tran);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(poNBRParm);
                cmd.Parameters.Add(shpmtNBRParm);
                cmd.Parameters.Add(distroNBRParm);
                cmd.Parameters.Add(itemNBRParm);
              
                try
                {
                    //dbCon.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
                //finally
                //{
                //    dbCon.Close();
                //}
            }
           
        }
        private String PopulateAAWorkList(string shpmtNBR, string distroNBR, string poNBR, string itemNBR, string skuNBR, OracleConnection dbCon,OracleTransaction tran)
        {
            StringBuilder strCombination = null;
            if (!String.IsNullOrEmpty(shpmtNBR) && !String.IsNullOrEmpty(distroNBR) && !String.IsNullOrEmpty(poNBR) && !String.IsNullOrEmpty(itemNBR))
            {
                OracleParameter poNBRParm = new OracleParameter("p_PO_NBR", OracleType.VarChar);
                poNBRParm.Direction = ParameterDirection.Input;
                poNBRParm.SourceVersion = DataRowVersion.Current;
                poNBRParm.Value = poNBR;

                OracleParameter shpmtNBRParm = new OracleParameter("p_ASN_NBR", OracleType.VarChar);
                shpmtNBRParm.Direction = ParameterDirection.Input;
                shpmtNBRParm.SourceVersion = DataRowVersion.Current;
                shpmtNBRParm.Value = shpmtNBR;

                OracleParameter distroNBRParm = new OracleParameter("p_ALLOC_NBR", OracleType.VarChar);
                distroNBRParm.Direction = ParameterDirection.Input;
                distroNBRParm.SourceVersion = DataRowVersion.Current;
                distroNBRParm.Value = distroNBR;

             
                OracleParameter itemNBRParm = new OracleParameter("p_ITM_NBR", OracleType.VarChar);
                itemNBRParm.Direction = ParameterDirection.Input;
                itemNBRParm.SourceVersion = DataRowVersion.Current;
                itemNBRParm.Value = itemNBR;

                OracleParameter skuNBRParm = new OracleParameter("p_SKU_NBR", OracleType.VarChar);
                skuNBRParm.Direction = ParameterDirection.Input;
                skuNBRParm.SourceVersion = DataRowVersion.Current;
                skuNBRParm.Value = skuNBR;

                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Output;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Output;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;

                //OracleParameter merchIDTypeParm = new OracleParameter("p_MERCH_ID_TYPE", OracleType.Number);
                //merchIDTypeParm.Direction = ParameterDirection.Output;
                //merchIDTypeParm.SourceVersion = DataRowVersion.Current;

                //OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);
                OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_WORKLIST", dbCon,tran);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(poNBRParm);
                cmd.Parameters.Add(shpmtNBRParm);
                cmd.Parameters.Add(distroNBRParm);          
                cmd.Parameters.Add(skuNBRParm);
                cmd.Parameters.Add(itemNBRParm);
                cmd.Parameters.Add(oldWLKeyParm);
                cmd.Parameters.Add(newWLKeyParm);
               // cmd.Parameters.Add(merchIDTypeParm);

                try
                {
                    //dbCon.Open();
                    cmd.ExecuteNonQuery();
                   
                    string oldWLKey = cmd.Parameters["p_OLD_WL_KEY"].Value.ToString();
                    string newWLKey = cmd.Parameters["p_NEW_WL_KEY"].Value.ToString();
                  // string merchIDKey = cmd.Parameters["p_MERCH_ID_TYPE"].Value.ToString();

                    strCombination = new StringBuilder();
                    strCombination.Append(oldWLKey);
                    strCombination.Append("|");
                    strCombination.Append(newWLKey);
                    strCombination.Append("|");
                  //  strCombination.Append(merchIDKey);

                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
                //finally
                //{
                //    dbCon.Close();
                //}
            }
            return strCombination.ToString();
        }

        private String PopulateAAWorkList_B(string distroNBR, string itemNBR, string skuNBR, OracleConnection dbCon, OracleTransaction tran)
        {
            StringBuilder strCombination = null;
            if (!String.IsNullOrEmpty(distroNBR) && !String.IsNullOrEmpty(itemNBR))
            {

                
                OracleParameter distroNBRParm = new OracleParameter("p_ALLOC_NBR", OracleType.VarChar);
                distroNBRParm.Direction = ParameterDirection.Input;
                distroNBRParm.SourceVersion = DataRowVersion.Current;
                distroNBRParm.Value = distroNBR;

                OracleParameter itemNBRParm = new OracleParameter("p_ITM_NBR", OracleType.VarChar);
                itemNBRParm.Direction = ParameterDirection.Input;
                itemNBRParm.SourceVersion = DataRowVersion.Current;
                itemNBRParm.Value = itemNBR;

                OracleParameter skuNBRParm = new OracleParameter("p_SKU_NBR", OracleType.VarChar);
                skuNBRParm.Direction = ParameterDirection.Input;
                skuNBRParm.SourceVersion = DataRowVersion.Current;
                skuNBRParm.Value = skuNBR;


                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Output;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Output;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;

                //OracleParameter merchIDTypeParm = new OracleParameter("p_MERCH_ID_TYPE", OracleType.Number);
                //merchIDTypeParm.Direction = ParameterDirection.Output;
                //merchIDTypeParm.SourceVersion = DataRowVersion.Current;

                OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_BACKSTOCK_WORKLIST", dbCon, tran);

                cmd.CommandType = CommandType.StoredProcedure;
               
                cmd.Parameters.Add(distroNBRParm);              
                cmd.Parameters.Add(itemNBRParm);
                cmd.Parameters.Add(skuNBRParm);
                cmd.Parameters.Add(oldWLKeyParm);
                cmd.Parameters.Add(newWLKeyParm);
              

                try
                {
                    //dbCon.Open();
                    cmd.ExecuteNonQuery();

                    string oldWLKey = cmd.Parameters["p_OLD_WL_KEY"].Value.ToString();
                    string newWLKey = cmd.Parameters["p_NEW_WL_KEY"].Value.ToString();
                    //string merchIDKey = cmd.Parameters["p_MERCH_ID_TYPE"].Value.ToString();

                    strCombination = new StringBuilder();
                    strCombination.Append(oldWLKey);
                    strCombination.Append("|");
                    strCombination.Append(newWLKey);
                    //strCombination.Append("|");
                    //strCombination.Append(merchIDKey);

                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
               
            }
            return strCombination.ToString();
        }

        private void PopulateWorkListBulk(Int32 oldWLKey, Int32 newWLKey, string skuNbr, Int32 qtyRcv, OracleConnection dbCon,OracleTransaction tran)
        {
            if (!Convert.IsDBNull(oldWLKey) && !Convert.IsDBNull(newWLKey) && !String.IsNullOrEmpty(skuNbr) && !Convert.IsDBNull(qtyRcv))
            {
                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Input;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;
                oldWLKeyParm.Value = oldWLKey;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Input;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;
                newWLKeyParm.Value = newWLKey;

                OracleParameter skuNBRParm = new OracleParameter("p_SKU_NBR", OracleType.VarChar);
                skuNBRParm.Direction = ParameterDirection.Input;
                skuNBRParm.SourceVersion = DataRowVersion.Current;
                skuNBRParm.Value = skuNbr;

                OracleParameter qtyRcvParm = new OracleParameter("p_REC_QTY", OracleType.Number);
                qtyRcvParm.Direction = ParameterDirection.Input;
                qtyRcvParm.SourceVersion = DataRowVersion.Current;
                qtyRcvParm.Value = qtyRcv;

                //OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);
                OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_WL_BULK", dbCon,tran);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(oldWLKeyParm);
                cmd.Parameters.Add(newWLKeyParm);
                cmd.Parameters.Add(skuNBRParm);
                cmd.Parameters.Add(qtyRcvParm);

                try
                {
                    //dbCon.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
                //finally
                //{
                //    dbCon.Close();
                //}
            }
        }

        //New function to populate WL_PACK table instead

        private void PopulateWorkListPack1(Int32 oldWLKey, Int32 newWLKey, string skuNbr, Int32 qtyRcv, OracleConnection dbCon, OracleTransaction tran)
        {
            if (!Convert.IsDBNull(oldWLKey) && !Convert.IsDBNull(newWLKey) && !String.IsNullOrEmpty(skuNbr) && !Convert.IsDBNull(qtyRcv))
            {
                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Input;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;
                oldWLKeyParm.Value = oldWLKey;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Input;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;
                newWLKeyParm.Value = newWLKey;

                OracleParameter skuNBRParm = new OracleParameter("p_SKU_NBR", OracleType.VarChar);
                skuNBRParm.Direction = ParameterDirection.Input;
                skuNBRParm.SourceVersion = DataRowVersion.Current;
                skuNBRParm.Value = skuNbr;

                OracleParameter qtyRcvParm = new OracleParameter("p_REC_QTY", OracleType.Number);
                qtyRcvParm.Direction = ParameterDirection.Input;
                qtyRcvParm.SourceVersion = DataRowVersion.Current;
                qtyRcvParm.Value = qtyRcv;

                
                OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_WL_PACK", dbCon,tran); 
              

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(oldWLKeyParm);
                cmd.Parameters.Add(newWLKeyParm);
                cmd.Parameters.Add(skuNBRParm);
                cmd.Parameters.Add(qtyRcvParm);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
               
            }
        }

        private void PopulateWorkListPack1_B(Int32 oldWLKey, Int32 newWLKey, string skuNbr, Int32 qtyBack, OracleConnection dbCon, OracleTransaction tran)
        {
            if (!Convert.IsDBNull(oldWLKey) && !Convert.IsDBNull(newWLKey) && !String.IsNullOrEmpty(skuNbr) && !Convert.IsDBNull(qtyBack))
            {
                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Input;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;
                oldWLKeyParm.Value = oldWLKey;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Input;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;
                newWLKeyParm.Value = newWLKey;

                OracleParameter skuNBRParm = new OracleParameter("p_SKU_NBR", OracleType.VarChar);
                skuNBRParm.Direction = ParameterDirection.Input;
                skuNBRParm.SourceVersion = DataRowVersion.Current;
                skuNBRParm.Value = skuNbr;

                OracleParameter qtyRcvParm = new OracleParameter("p_BACK_QTY", OracleType.Number);
                qtyRcvParm.Direction = ParameterDirection.Input;
                qtyRcvParm.SourceVersion = DataRowVersion.Current;
                qtyRcvParm.Value = qtyBack;


                OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_BACKSTOCK_WL_PACK", dbCon, tran);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(oldWLKeyParm);
                cmd.Parameters.Add(newWLKeyParm);
                cmd.Parameters.Add(skuNBRParm);
                cmd.Parameters.Add(qtyRcvParm);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }

            }
        }
        private void PopulateWorkListPack(Int32 oldWLKey, Int32 newWLKey, string skuNbr, Int32 qtyRcv, OracleConnection dbCon,OracleTransaction tran)
        {
            if (!Convert.IsDBNull(oldWLKey) && !Convert.IsDBNull(newWLKey) && !String.IsNullOrEmpty(skuNbr) && !Convert.IsDBNull(qtyRcv))
            {
                OracleParameter oldWLKeyParm = new OracleParameter("p_OLD_WL_KEY", OracleType.Number);
                oldWLKeyParm.Direction = ParameterDirection.Input;
                oldWLKeyParm.SourceVersion = DataRowVersion.Current;
                oldWLKeyParm.Value = oldWLKey;

                OracleParameter newWLKeyParm = new OracleParameter("p_NEW_WL_KEY", OracleType.Number);
                newWLKeyParm.Direction = ParameterDirection.Input;
                newWLKeyParm.SourceVersion = DataRowVersion.Current;
                newWLKeyParm.Value = newWLKey;

                OracleParameter skuNBRParm = new OracleParameter("p_SKU_NBR", OracleType.VarChar);
                skuNBRParm.Direction = ParameterDirection.Input;
                skuNBRParm.SourceVersion = DataRowVersion.Current;
                skuNBRParm.Value = skuNbr;

                OracleParameter qtyRcvParm = new OracleParameter("p_REC_QTY", OracleType.Number);
                qtyRcvParm.Direction = ParameterDirection.Input;
                qtyRcvParm.SourceVersion = DataRowVersion.Current;
                qtyRcvParm.Value = qtyRcv;

                //OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);

                // KJZ 10/21/2009: Commented out CREATE_WL_PACK line below and replaced with call to WL_MULTIPLE. 
                // The current HT_RECEIVE package on Arthur does not contain the proc CREATE_WL_PACK 
                // as Pre-Pack functionality was not implemented.  Thus, the correct procedure to be called
                // is CREATE_WL_MULTIPLE as is now implemented in the below code change

                //OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_WL_PACK", dbCon,tran); 
                OracleCommand cmd = new OracleCommand("HT_RECEIVE.CREATE_WL_MULTIPLE", dbCon, tran);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(oldWLKeyParm);
                cmd.Parameters.Add(newWLKeyParm);
                cmd.Parameters.Add(skuNBRParm);
                cmd.Parameters.Add(qtyRcvParm);

                try
                {
                    //dbCon.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
                //finally
                //{
                //    dbCon.Close();
                //}
            }
        }
        private void RecalculateWorkList(Int32 WLKey, Int32 merchIDType, OracleConnection dbCon,OracleTransaction tran)
        {
            if (!Convert.IsDBNull(WLKey) && !Convert.IsDBNull(merchIDType))
            {
                OracleParameter WLKeyParm = new OracleParameter("p_WL_KEY", OracleType.Number);
                WLKeyParm.Direction = ParameterDirection.Input;
                WLKeyParm.SourceVersion = DataRowVersion.Current;
                WLKeyParm.Value = WLKey;

                OracleParameter merchIDTypeParm = new OracleParameter("p_MERCH_ID_TYPE", OracleType.Number);
                merchIDTypeParm.Direction = ParameterDirection.Input;
                merchIDTypeParm.SourceVersion = DataRowVersion.Current;
                merchIDTypeParm.Value = merchIDType;


                //OracleConnection dbCon = new OracleConnection(Arthur_ConnStr);
                OracleCommand cmd = new OracleCommand("HT_RECEIVE.RECALCULATE_WORKLIST", dbCon,tran);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(WLKeyParm);
                cmd.Parameters.Add(merchIDTypeParm);

                try
                {
                    //dbCon.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    flLogFile.WriteItem(ErrorHandler.GetErrorMessage(ex));
                    throw;
                }
                //finally
                //{
                //    dbCon.Close();
                //}
            }
            
        }
        private void SendEmail(string status,string reason, int mode)
        {
            
            EmailUtil emailUtil = new EmailUtil(ConfigurationManager.AppSettings["SmtpServer"]);
            String CADC_ConnStr = ConfigurationManager.AppSettings["CADC_ConnStr"];
            if (mode == 1)
            {
                emailUtil.Subject = "Arthur ASN Discrepancy Report";
            }
            else
            {
                emailUtil.Subject = "Arthur Backstock Discrepancy Report";
            }
            emailUtil.AddReceivepient(ConfigurationManager.AppSettings["SendTo"]);
            emailUtil.SendFrom = "DoNotReply@hottopic.com";
            emailUtil.Priority = System.Net.Mail.MailPriority.High;
            string body = ConfigurationManager.AppSettings["EmailBody"];

            body = body.Replace("%processDate%", DateTime.Now.ToLongDateString());
            body = body.Replace("%status%", status);
            body = body.Replace("%reason%", reason);
            body = body.Replace("%SHIPMENT%", shpmt_NBR);
            body = body.Replace("%DISTRO%", distro_NBR);
            body = body.Replace("%PO%", po_NBR);
            body = body.Replace("%ITEM%", item_NBR);

            emailUtil.Body = body;
            emailUtil.HTMLFormat = true;

            emailUtil.Send();
        }
    }
}
