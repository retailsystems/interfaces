using System;
using System.Collections.Generic;
using System.Text;
using HottopicUtilities;

namespace AADiscrepancy
{
    class Program
    {
        static String fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath) + "\\" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".log";
        static LogUtil flLogFile = null;
        static void Main(string[] args)
        {
            flLogFile = GetLogFile();
            DiscrepancyManager dM = new DiscrepancyManager();
            dM.Process();
        }
        static public LogUtil GetLogFile()
        {
            if (flLogFile == null)
                flLogFile = new LogUtil();

            return flLogFile;
        }
    }
}
