<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageDistroPriority.aspx.vb" Inherits="BackStock.ManageDistroPriority"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ManageDistroPriority</title> 
		<!-- #include file="include/header.inc" -->
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="700" align="center" border="0">
				<tr height="15">
					<td class="pagecaption" align="middle">Manage Store Distro Priority
					</td>
				</tr>
				<tr height="15">
					<td align="middle"><asp:label id="lblError" CssClass="errStyle" Runat="server"></asp:label></td>
				</tr>
				<tr height="5">
					<td></td>
				</tr>
				<tr>
					<td align="middle" width="100%">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="500" align="center" border="0">
							<TR height="5">
								<TD align="middle" colSpan="3"><asp:validationsummary id="ValidationSummary1" CssClass="ErrStyle" Runat="server" DisplayMode="BulletList" EnableClientScript="true"></asp:validationsummary></TD>
							</TR>
							<TR>
								<TD class="cellvaluecaption" align="right" width="250">Priority Code:</TD>
								<TD width="10"><FONT class="reqStyle">*</FONT></TD>
								<TD width="240"><asp:dropdownlist id="lstPrtyCode" Runat="server" CssClass="cellvalueleft" AutoPostBack="True">
									
									</asp:dropdownlist></TD>
							</TR>
							<TR height="5">
								<TD colSpan="3"></TD>
							</TR>
							<TR>
								<TD align="middle" colSpan="3">
									<table width="100%" align="center">
										<tr>
											<td align="right" width="235" class="cellvaluecaption">
												Stores:<br>
												<asp:listbox id="lstStoresAll" CssClass="cellvalueleft" Runat="server" Width="235" SelectionMode="Multiple" Height="200"></asp:listbox></td>
											<td width="30" valign="center" align="middle">
												<asp:ImageButton ID="moveRight" Runat="server" ImageUrl="Images/MoveRight.gif" AlternateText="Add Store(s)"></asp:ImageButton>
												<asp:ImageButton ID="moveLeft" Runat="server" ImageUrl="Images/MoveLeft.gif" AlternateText="Delete Store(s)"></asp:ImageButton>
											</td>
											<td width="235" class="cellvaluecaption">Selected Stores:<br>
												<asp:ListBox id="lstStores" Runat="server" Height="200" CssClass="cellvalueleft" Width="235" SelectionMode="Multiple"></asp:ListBox>
											</td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR height="5">
								<TD colSpan="3"></TD>
							</TR>
							<TR>
								<TD align="middle" colSpan="3">
									<asp:Button id="btnSubmit" CssClass="B1" Runat="server" Text="Save"></asp:Button>
									<input type="button" class="B1" value="Reset" name="btnReset" onclick="document.location=document.location.href"></TD>
							</TR>
							<TR height="5">
								<TD colSpan="3"></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
