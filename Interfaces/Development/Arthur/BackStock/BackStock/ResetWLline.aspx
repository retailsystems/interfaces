<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ResetWLline.aspx.vb" Inherits="BackStock.ResetWLline" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<!-- #include file="include/header.inc" -->    
<script language="javascript">            
			function CancelDistro(AllocNbr){
				//alert(AllocNbr);
				if (confirm('Are you sure to cancel Allocation #' + AllocNbr + '?')){
					document.form1.hdnAllocNbr.value = AllocNbr;
					showWait();
					document.form1.btnCancel.click();					
				}
			}
			function submitDistro(AllocNbr){
				//alert(AllocNbr);
				if (confirm('Are you sure to Reset the worklist lines for allocation #' + AllocNbr + '?')){
					document.form1.hdnAllocNbr.value = AllocNbr;
					showWait();
					document.form1.btnResetWLlines.click();					
				}
			}
		</script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Allocation Number
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3"> 
							<TD align="center" style="height: 3px">
							    <asp:label id="lblError" Runat="server" CssClass="errStyle" Visible="False"></asp:label>
								<asp:validationsummary id="ValidationSummary1" Runat="server" CssClass="errStyle" EnableClientScript="false" DisplayMode="BulletList" ShowMessageBox="False"></asp:validationsummary>
							</TD>
							</TR>
							<tr>								
								<td class="cellvaluecaption" align="center">								
								Allocation 
									#:
									<asp:textbox id="txtAllocationNbr" Runat="server" CssClass="cellvalueleft" Width="170px" MaxLength="20"></asp:textbox>
									<asp:comparevalidator id="validateAllocationNbr" Runat="server" EnableClientScript="false" ControlToValidate="txtAllocationNbr" Display="None" ErrorMessage="Invalid Allocation #." Type="Double" Operator="DataTypeCheck"></asp:comparevalidator>
									<asp:requiredfieldvalidator id="reqAllocNbr" Runat="server" CssClass="reqStyle" EnableClientScript="false" ControlToValidate="txtAllocationNbr" Display="None" ErrorMessage="Please enter Allocation #." InitialValue=""></asp:requiredfieldvalidator>
									<asp:button id="btnSubmit" Runat="server" CssClass="btnSmall" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="btnSmall" id="btnClear" onclick="location.href=location.href" type="button" value="Clear" name="btnClear">
								</td>
							</tr>
							<!--
							<TR height="5">
								<TD class="cellvaluecaption" align="center"><asp:checkbox id="chkCopyToArthur" Visible="true" Runat="server" Text="Populate this distro to Arthur worklist" Checked="True"></asp:checkbox></TD>
							</TR>
							-->
						</TABLE>
					</TD>
				</TR>
				<TR height="20">
					<TD></TD>
				</TR>
				</TABLE>
    <!--#include file="include/wait.inc"--><br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Results
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3">
								<TD align="center" style="height: 3px"></TD>
							</TR>
							<tr>
								<td align="center"><asp:panel id="pnlResults" Runat="server" Visible="False" EnableViewState="False">
										<DIV id="divResetWLlines" style="DISPLAY: none">
                                            &nbsp;<INPUT id="hdnAllocNbr" type="hidden" value="0" name="hdnAllocNbr" runat="server">&nbsp;
											<asp:Button id="btnCancel" Runat="server" Width="0px" Text="" Height="0px"></asp:Button>
											<asp:Button id="btnResetWLlines" Runat="server" Width="0px" Text="" Height="0px"></asp:Button></DIV>
										<%If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then%>
										<TABLE class="DatagridSm" id="Table6" style="BORDER-COLLAPSE: collapse" cellSpacing="0"
											cellPadding="2" width="550" align="center" border="1">
											<TR class="DATAGRID_HeaderSm">
												<TD width="75">ALLOCATION #</TD>
												<TD width="75">WL KEY</TD>
												<TD width="75">AVAILABLE QTY</TD>
												<TD width="75">PO NUMBER</TD>
												<TD width="75">ITEM NUMBER</TD>
											</TR>
											<%
												dim rowStyle as string = "BC333333sm"
											    Dim i as int16 = 0	
											    Dim cancelButton As String = "<TD align=left ><input type=button class=btnSmallgrey value=Cancel onClick=""CancelDistro('" & hdnAllocNbr.Value & "')""></TD>"
											    Dim ConfirmToResetWLButton As String = "<TD align=left ><input type=button class=btnSmallgrey value='Reset WL lines' onClick=""submitDistro('" & hdnAllocNbr.Value & "')""></TD>"
											for each dr in ds.tables(0).rows
											%>
											
											<TR class="<%=rowStyle%>">
												<TD vAlign="middle"><%=dr("Alloc_Nbr")%></TD>
												<TD vAlign="middle"><%=dr("WL_Key")%></TD>
												<TD vAlign="middle"><%=dr("Avail_Qty")%></TD>
												<TD vAlign="middle"><%=dr("PO_NBR")%></TD>
												<TD vAlign="middle"><%=dr("Itm_Nbr")%></TD>
											
											<%								
											next
											'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "')""></TD></TR>")
											'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "','N')"">&nbsp;&nbsp;&nbsp;&nbsp;<input type=button class=btnSmallgrey value=Cancel&UpdateArthur " & cancelArthurBtnStatus & " onClick=""CancelAndUpdateArthur('" & prevDistroNbr & "','Y')""></TD></TR>")											
											%>																								
											</TR>											
											<tr><td></td><td></td><td></td><%Response.Write(cancelButton)%><%Response.Write(ConfirmToResetWLButton)%></tr>
										</TABLE>
										<%ElseIf (strErrorMsg.Length > 0) Then%>
										<FONT class="errStyle"><%Response.Write(strErrorMsg)%></FONT>
										<%Else%>
										<FONT class="errStyle">No records found.</FONT>
										<%END IF%>
									</asp:panel></td>
							</tr>							
						</TABLE>
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
			</TABLE>
    </form>
    <!-- #include file="include/footer.inc" -->
</body>
</html>
