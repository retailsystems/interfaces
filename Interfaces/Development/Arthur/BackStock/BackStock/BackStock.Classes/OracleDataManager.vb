Imports System.Data.OracleClient
Namespace SqlServices
    Public Enum EnumRSTypes As Byte
        DataSet = 1
        DataReader = 2
    End Enum
    Public Class OracleDataManager
        Public strConn As String
        Private _cn As OracleConnection
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub
        Private Sub GetConnectionString()
            strConn = "" ' set the connection string value here
        End Sub

        Public Function ExecuteReader(ByVal strSQL As String) As OracleDataReader
            Return OracleHelper.ExecuteReader(strConn, CommandType.Text, strSQL)
        End Function
        Public Function ExecuteReader(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As OracleDataReader
            Return OracleHelper.ExecuteReader(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function
        'Public Function ExcuteDataSet(ByVal strSQL As String) As DataSet
        '    Return OracleHelper.ExecuteDataset(strConn, CommandType.Text, strSQL)
        'End Function
        Public Function ExecuteDataSet(ByVal strSQL As String) As DataSet
            Return OracleHelper.ExecuteDataset(strConn, CommandType.Text, strSQL)
        End Function
        Public Function ExecuteDataSet(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As DataSet
            Return OracleHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function
        Public Function ExecuteNonQuery(ByVal strSQL As String) As Integer
            Return OracleHelper.ExecuteNonQuery(strConn, CommandType.Text, strSQL)
        End Function
        Public Function ExecuteNonQuery(ByVal spName As String, ByVal ParamArray commandParameters() As OracleParameter) As Integer
            Return OracleHelper.ExecuteNonQuery(strConn, CommandType.StoredProcedure, spName, commandParameters)
        End Function
    End Class
    Public Class AAOracleDataManager
        Inherits OracleDataManager
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub
        Private Sub GetConnectionString()
            MyBase.strConn = Backstock.Classes.AppSetting.AAConnectionString
        End Sub
    End Class
    Public Class WMSOracleDataManager
        Inherits OracleDataManager
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub
        Private Sub GetConnectionString()
            MyBase.strConn = Backstock.Classes.AppSetting.WMSConnectionString
        End Sub
    End Class
    ' 06/02/2005 SRI - TN DC Data Manager
    Public Class TNDCWMSOracleDataManager
        Inherits OracleDataManager
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub
        Private Sub GetConnectionString()
            MyBase.strConn = Backstock.Classes.AppSetting.TnDcWMSConnectionString
        End Sub
    End Class
    Public Class GersOracleDataManager
        Inherits OracleDataManager
        Private cnnSQL As OracleConnection
        Sub New()
            GetConnectionString()
        End Sub
        Private Sub GetConnectionString()
            MyBase.strConn = Backstock.Classes.AppSetting.GERSConnectionString
        End Sub
    End Class

End Namespace