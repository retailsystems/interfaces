<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StatusCheck.aspx.vb" Inherits="BackStock.StatusCheck"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="include/Styles.css" type="text/css" rel="stylesheet">
		<%if refreshPage then%>
		<meta http-equiv="refresh" content="200; url=<%=request.Url%>">
		<%end if%>
		<!-- #include file="include/PopupHeader.inc" -->
		<script language="javascript">
		function PrintPage()
		{
			window.print();
			return false;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="self.focus()" style="">
		<form id="Form1" method="post" runat="server">
			<table align="center" width="500" border="0" cellpadding="0" cellspacing="0" height="326"
				style="LEFT: 35px;POSITION: absolute; TOP: 15px">
				<tr>
					<td height="5" align="left"></td>
				</tr>
				<tr>
					<%If IsNumeric(Request("BatchID")) Then%>
					<td align="center" class="PageCaption">STATUS CHECK (BATCH #<%=request("batchID")%>)</td>
					<%else%>
					<td align="center" class="PageCaption">RESERVE UPDATE STATUS CHECK</td>
					<%end if%>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td align="center">
						<asp:Label Runat="server" ID="lblError" CssClass="errStyle" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td class="cellvaluewhite" align="center">
						Last checked on:
						<%=Now%>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<%If IsNumeric(Request("BatchID")) Then%>
					<td align="center">
						<asp:DataGrid ID="dgResults" Width="400px" runat="server" BorderColor="#990000" CellPadding="1"
							HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle"
							AlternatingItemStyle-CssClass="GridAlternateStyle" AutoGenerateColumns="false">
							<Columns>
								<asp:BoundColumn DataField="ALLOC_NBR" HeaderText="ALLOC_NBR"></asp:BoundColumn>
								<asp:BoundColumn DataField="Export_Status" HeaderText="Exported to WMS?"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
					<%else%>
					<td align="center">
						<asp:DataGrid ID="dgReserveUpdateStatus" Width="400px" runat="server" BorderColor="#990000" CellPadding="1"
							HeaderStyle-CssClass="GridHeaderStyle" CssClass="GridStyle" ItemStyle-CssClass="GridItemStyle"
							AlternatingItemStyle-CssClass="GridAlternateStyle" AllowPaging="True" PageSize="25" PagerStyle-Mode="NumericPages"
							AllowCustomPaging="False" OnPageIndexChanged="dgReserve_PageChanger"  PagerStyle-HorizontalAlign="Center" PagerStyle-Font-Name="Arial"
							PagerStyle-Font-Size="10px"></asp:DataGrid>
					</td>
					<%end if%>
				</tr>
				<tr>
					<td height="10" align="center"><asp:Button ID="btnPrint" Text="Print" CssClass="btnSmall" Runat="server" /></td>
				</tr>
				<tr>
					<font class="contentTextSmall"><sup>*</sup>NOTE:This page refreshes automatically 
						for every 3 minutes.</font> </td>
				</tr>
			</table>
		</form>
		<!-- #include file="include/PopupFooter.inc" -->
	</body>
</HTML>
