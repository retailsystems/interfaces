Imports BackStock.Classes
Partial Public Class AAComments
    Inherits System.Web.UI.Page
    Protected strErrorMsg As String = String.Empty
    Private _workFlowManager As New WorkFlowManager
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'PnlDelUpd.Visible = False
        'PnlInsert.Visible = False
        lblError.Text = ""
        lblError.Visible = False
    End Sub

    Protected Sub InsertButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If (txtItm_Nbr.Text.Trim().Length = 0) Then
            'Response.Write("<script language='javascript'>alert('Please enter the valid Item number');</script>")
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS"
        Else
            PnlDelUpd.Visible = True
            PnlInsert.Visible = False

            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS where ITM_NBR='" + txtItm_Nbr.Text.ToString().Trim() & "'"
        End If
    End Sub
    Private Function ValidateRequest() As Boolean
        Dim validReqFlag As Boolean = False
        If Page.IsValid AndAlso txtItm_Nbr.Text.Trim <> "" AndAlso IsNumeric(txtItm_Nbr.Text.Trim) = True Then
            validReqFlag = True
        End If
        Return validReqFlag
    End Function
    Protected Sub dtlvwAddNewCmt_OnItemInserting(ByVal sender As Object, ByVal e As DetailsViewInsertEventArgs)

        If (e.Values.Item("ITM_NBR") <> Nothing AndAlso e.Values.Item("TEXT") <> Nothing) Then
            If (e.Values.Item("ITM_NBR").ToString().Trim().Length > 0 AndAlso _workFlowManager.IsItemNbrInRMS(e.Values.Item("ITM_NBR").ToString().Trim()) = False) Then
                lblError.Text = "Item number not found in RMS. Please provide valid Item number for insert"
                ShowMsg("Following error(s) occured while processing your request!")
                e.Cancel = True
            End If
        Else
            lblError.Text = "Item number/TEXT cannot be null. Please provide valid Item number/TEXT for insert"
            ShowMsg("Following error(s) occured while processing your request!")
            e.Cancel = True
        End If
    End Sub
    Protected Sub dtlvwAddNewCmt_OnItemInserted(ByVal sender As Object, ByVal e As DetailsViewInsertedEventArgs)
        If (e.AffectedRows = 0) Then
            lblError.Text = "An error occurred while inserting the record into Style_Details table " + e.Exception.Message.ToString()
            ShowMsg("Following error(s) occured while processing your request!")
            e.KeepInInsertMode = True
            e.ExceptionHandled = True
        ElseIf (e.AffectedRows > 0) Then
            lblError.Text = "Successfully inserted item in Style_Details table"
            ShowMsg("Following error(s) occured while processing your request!")
        End If
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        'lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Text = msg & "<BR>" & lblError.Text
        lblError.Visible = True
    End Sub
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As EventArgs)
        dtlvwAddNewCmt.ChangeMode(DetailsViewMode.Insert)
        dtlvwAddNewCmt.DataSourceID = "sqlArthurDS"
    End Sub
    Protected Sub ItmStyleGridView_OnRowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        'GridView1.EditIndex = e.NewEditIndex;            
        If (txtItm_Nbr.Text.Length > 0) Then
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS where ITM_NBR='" + txtItm_Nbr.Text.ToString().Trim() & "'"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"
        Else
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"
        End If
    End Sub
    Protected Sub ItmStyleGridView_OnRowUpdated(ByVal sender As Object, ByVal e As GridViewUpdatedEventArgs)
        If (e.AffectedRows = 0) Then
            lblError.Text = "An error occurred while inserting the record into Style_Details table " + e.Exception.Message.ToString()
            ShowMsg("Following error(s) occured while processing your request!")
            e.KeepInEditMode = True
            e.ExceptionHandled = True
        End If

        If (txtItm_Nbr.Text.Trim().Length > 0) Then
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS where ITM_NBR='" + txtItm_Nbr.Text.ToString().Trim() & "'"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"
        Else
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"
        End If
    End Sub
    Protected Sub ItmStyleGridView_OnRowDeleted(ByVal sender As Object, ByVal e As GridViewDeletedEventArgs)
        If (e.AffectedRows > 0) Then
            lblError.Text = "Successfully delete the item"
            ShowMsg("Following error(s) occured while processing your request!")
        ElseIf (e.AffectedRows = 0) Then
            lblError.Text = "An error occurred while deleting the record in Style_Details table " + e.Exception.Message.ToString()
            ShowMsg("Following error(s) occured while processing your request!")
            e.ExceptionHandled = True
        End If

        If (txtItm_Nbr.Text.Trim().Length > 0) Then
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS where ITM_NBR='" + txtItm_Nbr.Text.ToString().Trim() & "'"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"
        Else
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"
        End If
    End Sub
    Protected Sub ItmStyleGridView_OnPageIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If (txtItm_Nbr.Text.Trim().Length > 0) Then

            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS where ITM_NBR='" + txtItm_Nbr.Text.ToString().Trim() & "'"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"

        Else
            sqlArthurDS.SelectCommand = "SELECT * FROM AAM.STYLE_DETAILS"
            grdvwStyleDetail.DataSourceID = "sqlArthurDS"

        End If
    End Sub

End Class