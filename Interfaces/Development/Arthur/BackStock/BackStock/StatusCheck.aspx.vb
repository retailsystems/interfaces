Imports BackStock.Classes
Imports System.Web.Caching
Imports System.Data
Partial Class StatusCheck
    Inherits System.Web.UI.Page
    Private _ds As DataSet
    Private _dr As DataRow
    Protected refreshPage As Boolean
    Private _batchID As Int16

    Private _workFlowManager As New WorkFlowManager
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        refreshPage = False
        lblError.Visible = False
        lblError.Text = ""
        If Not IsPostBack Then
            btnPrint.Attributes.Add("OnClick", "return window.print();")
            If IsNumeric(Request("BatchID")) Then
                _batchID = Request("BatchID")
                BindData()
            Else
                Select Case Request("BatchID").ToUpper()
                    Case "WRU"
                        BindReserveUpdate()
                End Select
            End If
        End If
    End Sub
    Sub dgReserve_PageChanger(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        dgReserveUpdateStatus.CurrentPageIndex = e.NewPageIndex
        BindReserveUpdate()
    End Sub

    Private Sub ShowMessage(ByVal msg As String)
        lblError.Text = msg
        lblError.Visible = True
    End Sub
    Private Sub BindReserveUpdate()
        Try
            If Cache.Get("Reserve") Is Nothing Then
                _ds = _workFlowManager.ReserveUpdateStatus()
                Cache.Insert("Reserve", _ds, Nothing, DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration)
            Else
                _ds = CType(Cache.Get("Reserve"), DataSet)
            End If
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                refreshPage = True
                dgReserveUpdateStatus.DataSource = _ds.Tables(0)
                dgReserveUpdateStatus.DataBind()
            Else
                ShowMessage("No records found.")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub
    Private Sub BindData()
        Try
            _ds = _workFlowManager.RunStatusCheckByBatch(_batchID)
            If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
                For Each _dr In _ds.Tables(0).Rows
                    If _dr("Export_Flag") = 0 Then
                        refreshPage = True
                        Exit For
                    End If
                Next
                dgResults.DataSource = _ds.Tables(0)
                dgResults.DataBind()
            Else
                ShowMessage("No records found")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub
End Class
