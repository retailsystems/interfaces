<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Home.aspx.vb" Inherits="BackStock.Home"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="javascript">
		//-------------------------------------------------------------
			// Select all the checkboxes 
			//-------------------------------------------------------------
			function SelectAllCheckboxes(cbDivName, cbChildName){ 
							
				var theBox =eval("document.Form1." + cbDivName);
				var xState =theBox.checked; 								
				elm=document.Form1.elements; 				
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && elm[i].name == cbChildName)
					{					
					elm[i].checked = xState;				
					}
									
			}
			function UpdateCheckedItemStr(){
				var checkedStr = "";
				elm=document.Form1.elements;					
				for(i=0;i<elm.length;i++)
				if(elm[i].type=="checkbox" && (elm[i].name == "cbHtdept" || elm[i].name == "cbTddept" || elm[i].name == "cbShdept" || elm[i].name == "cbCANdept"))
					{					
					if(elm[i].checked){
						if (checkedStr.length>0) checkedStr += ",";
						checkedStr += elm[i].value ;						
					}				
					}	
								
				document.Form1.hdnDeptIdStr.value = checkedStr;	
				//alert(document.Form1.hdnDeptIdStr.value);					
				if (!document.Form1.hdnDeptIdStr.value.length > 0) {
					alert("Please select Hot Topic/Torrid/Box Lunch/Hot Topic (Canada) department(s)");
					return false;
				}else{
					return true;
				}
				
			}
			
			function ShowProgessBar(actionType){
				var isValid = true;				
				if (actionType == "Batch" || actionType == "Update" || actionType == 'Clear'){				
					//validate batch data					
					isValid = UpdateCheckedItemStr();					
				}				
				if (isValid){
					document.Form1.btnReserveClear.style.display = "none";
					document.Form1.btnReserveUpdate.style.display = "none";				
					document.Form1.btnReserveBatch.style.display = "none";
					document.Form1.btnClearReserveByDept.style.display = "none";
					showWait();
				}				
				return isValid;
			}				
		</script>
		<!-- #include file="include/header.inc" -->
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<input id="hdnDeptIdStr" type="hidden" name="hdnDeptIdStr" runat="server">
			<table cellSpacing="0" cellPadding="0" width="800" border="0">
				<tr>
					<td class="PageCaption" align="center" colSpan="4">Backstock Administration Console
					</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>
			</table>
			<asp:panel id="pnlLock" Runat="server" Visible="False">
				<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
					cellSpacing="0" cellPadding="2" width="500" border="0">
					<TR height="30">
						<TD></TD>
					</TR>
					<TR>
						<TD class="contentDkText" align="center">
							<asp:Label id="lblLockMsg" Runat="server"></asp:Label>.<BR>
							click
							<asp:LinkButton id="lnkLock" Runat="server" CausesValidation="False">here</asp:LinkButton>&nbsp;to 
							unlock the application.
						</TD>
					</TR>
					<TR height="30">
						<TD></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="pnlMain" Runat="server">
				<TABLE cellSpacing="0" cellPadding="0" width="800" border="0">
					<TR>
						<TD align="center" colSpan="3"><!-- #include file="include/wait.inc" --></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="3">
							<asp:Label id="lblError" Visible="True" Runat="server" CssClass="errStyle"></asp:Label></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="3"><!-- #1/04/2006# Sri Bajjuri: B/S move changes --><FONT class="cellvaluecaption">Warehouse:</FONT>
							<asp:dropdownlist id="lstWhse" Runat="server" CssClass="cellvalueleft">
								<asp:ListItem Value="">--Select One--</asp:ListItem>
								<asp:ListItem Value="999">DC-Industry,CA</asp:ListItem>
								<asp:ListItem Value="997">DC-LaVergne,TN</asp:ListItem>
								<asp:ListItem Value="999|997">Both Warehouses</asp:ListItem>
							</asp:dropdownlist>
							<asp:requiredfieldvalidator id="reqValidator1" Runat="server" CssClass="reqStyle" ControlToValidate="lstWhse"
								Display="None" ErrorMessage="Please select a Warehouse." InitialValue="" EnableClientScript="false"></asp:requiredfieldvalidator></TD>
					</TR>
					<TR height="5">
						<TD align="center" colSpan="3"></TD>
					</TR>
					<TR>
						<TD vAlign="top" width="100%"><%' reserve batch %>
							<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
								cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
								<TR height="15">
									<TD class="cellvaluecaption" bgColor="#990000" height="23">&nbsp;Reserve Batch
									</TD>
								</TR>
								<TR height="5">
									<TD></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center">
										<TABLE borderColor="#990000" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR height="5">
												<TD colSpan="1"></TD>
											</TR>
											<TR>
												<TD class="contentDkText" colSpan="2">This function allows data to be transferred 
													from AA to WMS when backstock is allocated.
												</TD>
											</TR>
											<TR height="8">
												<TD colSpan="1"></TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD vAlign="top" height="100%">
																<TABLE style="BORDER-RIGHT: #659bad 1px solid; BORDER-TOP: #659bad 1px solid; BORDER-LEFT: #659bad 1px solid; BORDER-BOTTOM: #659bad 1px solid"
																	height="100%" cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
																	<TR height="15">
																		<TD class="blueBottomBorder" bgColor="#000000"><INPUT class="cellvalueleft" onclick="SelectAllCheckboxes('cbHt','cbHtdept')" type="checkbox"
																				name="cbHt"> <FONT class="cellvaluecaption">Hot Topic</FONT>
																		</TD>
																	</TR>
																	<TR height="3">
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center">
																			<TABLE borderColor="#659bad" height="100%" cellSpacing="0" cellPadding="0" width="100%"
																				border="0">
																				<TR height="3">
																					<TD colSpan="2"></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="2">
																						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																							<%
																						Dim i as integer = 1
																						 
																						If Not isNothing(Session("HtDeptDs")) AndAlso Session("HtDeptDs").tables(0).rows.count > 0 then
																							Response.write("<tr>")
																							for each dr in Session("HtDeptDs").tables(0).rows
																								If i = 1 then
																									Response.write("<tr>")
																								End If
																								Response.Write("<td class='cellvaluewhite' width='50%'>")
																								Response.Write("<input type='checkbox' name='cbHtdept' value='" & dr("DEPT_CD") & "'>" & dr("DEPT_CD") & " - " & dr("Description")   )
																								Response.Write("</td>" & vbNewLine)
																								IF i mod 2 = 0 then
																									Response.write("</tr>")
																									i = 1
																								Else
																									i += 1
																								End If
																							next
																						End if
																						
																					%>
																						</TABLE>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2%"></TD>
															<TD vAlign="top" height="100%">
																<TABLE style="BORDER-RIGHT: #659bad 1px solid; BORDER-TOP: #659bad 1px solid; BORDER-LEFT: #659bad 1px solid; BORDER-BOTTOM: #659bad 1px solid"
																	cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
																	<TR height="15">
																		<TD class="blueBottomBorder" bgColor="#000000"><INPUT class="cellvalueleft" onclick="SelectAllCheckboxes('cbTd','cbTddept')" type="checkbox"
																				name="cbTd"> <FONT class="cellvaluecaption">Torrid</FONT>
																		</TD>
																	</TR>
																	<TR height="3">
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center">
																			<TABLE borderColor="#659bad" height="100%" cellSpacing="0" cellPadding="0" width="100%"
																				border="0">
																				<TR height="3">
																					<TD></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top">
																						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<%
																						If Not isNothing(Session("TdDeptDs")) AndAlso Session("TdDeptDs").tables(0).rows.count > 0 then
																							Response.write("<tr>")
																							for each dr in Session("TdDeptDs").tables(0).rows
																								Response.Write("<td class='cellvaluewhite' width='50%'>")
																								Response.Write("<input type='checkbox' name='cbTddept' value='" & dr("DEPT_CD") & "'>" & dr("DEPT_CD") & " - " & dr("Description")   )																						
																								Response.Write("</td>")		
																								Response.write("</tr>" & vbNewLine)																			
																							next
																						End if
																						%>
																						</TABLE>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														
														<tr height="100">
															<td vAlign="top" height="100%">&nbsp;</td>
															<td vAlign="top" height="100%">&nbsp;</td>
															<td vAlign="top" height="100%">&nbsp;</td>
														</tr>
														
														<tr>
															<td vAlign="top" height="100%">
																<!-- Hottopic Canada -->
																<TABLE style="BORDER-RIGHT: #659bad 1px solid; BORDER-TOP: #659bad 1px solid; BORDER-LEFT: #659bad 1px solid; BORDER-BOTTOM: #659bad 1px solid"
																	height="100%" cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
																	<TR height="15">
																		<TD class="blueBottomBorder" bgColor="#000000"><INPUT class="cellvalueleft" onclick="SelectAllCheckboxes('cbCAN','cbCANdept')" type="checkbox"
																				name="cbCAN"> <FONT class="cellvaluecaption">Hot Topic (Canada)</FONT>
																		</TD>
																	</TR>
																	<TR height="3">
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center">
																			<TABLE borderColor="#659bad" height="100%" cellSpacing="0" cellPadding="0" width="100%"
																				border="0">
																				<TR height="3">
																					<TD colSpan="2"></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="2">
																						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<%
																						i = 1
																						
																						If Not isNothing(Session("CANDeptDs")) AndAlso Session("CANDeptDs").tables(0).rows.count > 0 then
																							Response.write("<tr>")
																							for each dr in Session("CANDeptDs").tables(0).rows
																								If i = 1 then
																									Response.write("<tr>")
																								End If
																								Response.Write("<td class='cellvaluewhite' width='50%'>")
																								Response.Write("<input type='checkbox' name='cbCANdept' value='" & dr("DEPT_CD") & "'>" & dr("DEPT_CD") & " - " & dr("Description")   )																						
																								Response.Write("</td>" & vbNewLine)		
																								IF i mod 2 = 0 then
																									Response.write("</tr>")
																									i = 1
																								Else
																									i += 1
																								End If																					
																							next
																						End if
																					    %>
																						</TABLE>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</td>
															<td vAlign="top" height="100%"></td>
															<td vAlign="top" height="100%">
																<!-- BoxLunch -->
																<TABLE style="BORDER-RIGHT: #659bad 1px solid; BORDER-TOP: #659bad 1px solid; BORDER-LEFT: #659bad 1px solid; BORDER-BOTTOM: #659bad 1px solid"
																	cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
																	<TR height="15">
																		<TD class="blueBottomBorder" bgColor="#000000"><INPUT class="cellvalueleft" onclick="SelectAllCheckboxes('cbSh','cbShdept')" type="checkbox"
																				name="cbSh"> <FONT class="cellvaluecaption">Box Lunch</FONT>
																		</TD>
																	</TR>
																	<TR height="3">
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center">
																			<TABLE borderColor="#659bad" height="100%" cellSpacing="0" cellPadding="0" width="100%"
																				border="0">
																				<TR height="3">
																					<TD></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top">
																						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<%
																						If Not isNothing(Session("ShDeptDs")) AndAlso Session("ShDeptDs").tables(0).rows.count > 0 then
																							Response.write("<tr>")
																							for each dr in Session("ShDeptDs").tables(0).rows
																								Response.write("<tr>")
																								Response.Write("<td class='cellvaluewhite' width='50%'>")
																								Response.Write("<input type='checkbox' name='cbShdept' value='" & dr("DEPT_CD") & "'>" & dr("DEPT_CD") & " - " & dr("Description")   )
																								Response.Write("</td>")																								
																								Response.write("</tr>" & vbNewLine)																								
																							next
																						End if
																						
																						%>
																						</TABLE>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															
															</td>
														</tr>
														
														
													</TABLE>
												</TD>
											</TR>
											<TR height="10">
												<TD colSpan="1"></TD>
											</TR>
											<TR>
												<TD vAlign="bottom" align="center" height="23">												
													<asp:button id="btnReserveUpdate" Runat="server" CausesValidation="True" CssClass="btnSmall"
														Text="Run Reserve Update"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:button id="btnReserveBatch" Runat="server" CausesValidation="True" CssClass="btnSmall"
														Text="Run Reserve Batch"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:Button id="btnClearReserveByDept" runat="server" CausesValidation="True" CssClass="btnSmall"
														Text="Run Reserve Clear" Visible="false"></asp:Button></TD>
											<TR height="5">
												<TD colSpan="1"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR height="5">
									<TD></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<SCRIPT language="JavaScript1.1">
					<!--
					function PreSelectCheckboxes(){
					//alert(document.Form1.hdnDeptIdStr.value);
					
						var checkedStr = document.Form1.hdnDeptIdStr.value;
						if (checkedStr.length > 0){
							var separator = ',';
							var stringArray = checkedStr.split(separator);
							for (var i=0; i < stringArray.length; i++){	
								//alert(stringArray[i]);					
								SelectCheckboxByvalue(stringArray[i]);
							}
						}
					
					}
					function SelectCheckboxByvalue(val){
						elm=document.Form1.elements;					
						for(i=0;i<elm.length;i++)
						if(elm[i].type=="checkbox" && (elm[i].name == "cbHtdept" || elm[i].name == "cbTddept" || elm[i].name == "cbShdept" || elm[i].name == "cbCANdept")  )
							{					
								if(elm[i].value == val){
									elm[i].checked = true;	
									break;				
								}				
							}	
					}
					window.onload = PreSelectCheckboxes;
					
					//-->
				</SCRIPT>
			</asp:panel></form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
