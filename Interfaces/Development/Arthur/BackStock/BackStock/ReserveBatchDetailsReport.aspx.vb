Imports BackStock.Classes
Partial Class ReserveBatchDetailsReport
    Inherits System.Web.UI.Page
    Private _ds, _ds1 As DataSet
    Private _workFlowManager As New WorkFlowManager
    Private _fileName As String
    Private _warehouseNbr As Int16
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Response.ContentType = "Application/x-msexcel"
        _fileName = "BATCH_#" & Request("batchID") & ".xls"
        _warehouseNbr = IIf(IsNumeric(Request("WarehouseNbr").Trim), Request("WarehouseNbr").Trim, 0)
        Response.AppendHeader("content-disposition", _
                "attachment; filename=" & _fileName)
        RunReserveBatchReport()
        'Response.Flush()
        'Response.Close()
    End Sub
    Public Sub RunReserveBatchReport()
        'Response.ContentType = "Application/x-msexcel"
        Try
            Dim dr() As DataRow

            _ds = _workFlowManager.RunReserveBatchReport(Request("batchID"), _warehouseNbr)
            If _ds.Tables.Count > 0 Then
                'dgAA.DataSource = _ds.Tables("AAAllocs")
                'Add the required fields
                _ds.Tables(0).Columns.Add("REQD_QTY", Type.GetType("System.Decimal"))
                _ds.Tables(0).Columns.Add("Variance", Type.GetType("System.Decimal"))

                'Merge and Evaluate Variance column value
                For Each row As DataRow In _ds.Tables(0).Rows
                    dr = _ds.Tables("WMDistros").Select("SKU_NBR = '" + row("SKU_NBR").ToString() + "' and ALLOC_NBR='" + row("ALLOC_NBR").ToString() + "' and DEPT_NBR ='" + row("DEPT_NBR").ToString() + "'")
                    If (dr.Length > 0) Then
                        row("REQD_QTY") = dr(0).ItemArray(3)
                    Else
                        row("REQD_QTY") = 0
                    End If
                    If (row("QTY") Is DBNull.Value) Then
                        row("QTY") = 0
                    End If
                    row("Variance") = row("QTY") - row("REQD_QTY")
                Next row

                dgAA.DataSource = _ds.Tables(0)
                dgAA.DataBind()

                'Enable the below binding for troubleshooting purposes
                'dgWMS.DataSource = _ds.Tables("WMDistros")
                'dgWMS.DataBind()
                dgWMS.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class
