Imports BackStock.Classes
Partial Class Login
    Inherits System.Web.UI.Page
    Protected ds As New DataSet
    Private _workFlowManager As New WorkFlowManager
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblError.Text = ""
        lblError.Visible = False
    End Sub

    Private Sub cmdSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        If Page.IsValid Then
            Try
                Dim _userName As String
                Session("UserName") = ""
                _userName = _workFlowManager.ValidateUser(txtUserName.Text.Trim, txtPassword.Text.Trim)
                Session("UserName") = _userName
                Response.Redirect("Home.aspx")
            Catch ex As BackStockAppException
                ShowMsg(ex.Message)
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text = msg
        lblError.Visible = True
    End Sub
End Class
