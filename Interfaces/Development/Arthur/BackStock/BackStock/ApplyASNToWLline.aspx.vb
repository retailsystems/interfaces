Imports BackStock.Classes
Partial Public Class ApplyASNToWLline
    Inherits System.Web.UI.Page
    Protected ds As New DataSet
    Protected dr As DataRow
    Protected strErrorMsg As String = String.Empty
    Private _workFlowManager As New WorkFlowManager

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        lblError.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindData()
    End Sub
    Private Sub BindData()
        Try
            If ValidateRequest() Then
                'Check the allocation/WMS number does exist in Arthur
                ds = _workFlowManager.GetWLLine(txtWL_Key.Text.Trim, txtASN.Text.Trim(), Session("UserName"), strErrorMsg)
                'If (strErrorMsg.Length > 0) Then
                'ShowMsg(strErrorMsg)
                'End If
                pnlResults.Visible = True
                hdnWL_Key.Value = txtWL_Key.Text.ToString()
                hdnASN.Value = txtASN.Text.ToString()
            Else
                'pnlResults.Visible = False
                ShowMsg("Following error(s) occured while processing your request!")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Function ValidateRequest() As Boolean
        Dim validReqFlag As Boolean = False
        If Page.IsValid AndAlso txtWL_Key.Text.Trim <> "" AndAlso txtASN.Text.Trim <> "" Then
            validReqFlag = True
        End If
        Return validReqFlag
    End Function
    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Visible = True
    End Sub

    Protected Sub btnUpdateASN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateASN.Click
        Try
            Dim strErrorMsg As String = String.Empty
            If (hdnWL_Key.Value.Trim().Length > 0 AndAlso hdnASN.Value.Trim().Length > 0) Then
                ds = _workFlowManager.SetASNonWLLine(hdnWL_Key.Value.Trim(), hdnASN.Value.Trim(), Session("UserName"), strErrorMsg)
                ShowMsg(strErrorMsg)
                pnlResults.Visible = True
            Else
                ShowMsg("Invalid WL_Key " & hdnWL_Key.Value & " and ASN # " & hdnASN.Value & " submitted for Reset")
            End If

        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./ApplyASNToWLline.aspx")
    End Sub
End Class