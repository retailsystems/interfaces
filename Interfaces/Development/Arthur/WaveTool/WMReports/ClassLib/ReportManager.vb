Imports System.Web
Imports HotTopic.DCSS.Services
Imports HotTopic.AIMS.Logging
Imports System.Data.OracleClient


Public Class ReportManager
    Private fileName As String
    Private flLogFile As FileLogger
    Private _cn, _aaCn As OracleDataManager
    Private _connStr, _aaConnStr As String
    'Private _dbSchema As String = ""
    Private _strSql As String
    'Public Sub New()

    'End Sub
    Public Sub New(ByVal whse As Int16)
        SetWhseDBConnectionByID(whse)
        fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath) & "\" & DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") & ".log"
        flLogFile = New FileLogger(fileName)
    End Sub
    Private Sub SetWhseDBConnectionByID(ByVal whse As Int16)
        _aaConnStr = HttpContext.Current.Application("Arthur_ConnStr")
        Select Case whse
            Case 999
                _connStr = HttpContext.Current.Application("CADC_ConnStr")
                '_dbSchema = HttpContext.Current.Application("CADC_DbSchema")
            Case 997
                _connStr = HttpContext.Current.Application("TNDC_ConnStr")
                '_dbSchema = HttpContext.Current.Application("TNDC_DbSchema")
        End Select
        'If Not _dbSchema.EndsWith(".") Then
        '    _dbSchema &= IIf(_dbSchema.Trim <> "", ".", "")
        'End If
    End Sub
    Public Function GetShipmentHdrInfo(ByVal searchParams As WaveSearchParams) As DataSet
        Try
            Dim ds As DataSet
            If _connStr = "" Then
                SetWhseDBConnectionByID(searchParams.whse)
            End If
            _cn = New OracleDataManager(_connStr)
            '_strSql = BuildWaveToolSql(searchParams)
            '_strSql = "SELECT SHPMT_NBR,PO_NBR,FIRST_RCPT_DATE_TIME,CODE_DESC FROM " & _
            '   " %DbSchema%ASN_HDR A JOIN %DbSchema%SYS_CODE S ON A.STAT_CODE = S.CODE_ID " & _
            '   " AND S.CODE_TYPE = '564' AND S.REC_TYPE = 'S'" & _
            '   " WHERE SHPMT_NBR = '" & searchParams.ShipmentNbr & "'"
            '_strSql = _strSql.Replace("%DbSchema%", _dbSchema)
            'ds = _cn.ExecuteDataSet(_strSql)
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = searchParams.ShipmentNbr
            cmdParameters(1) = New OracleParameter("AsnInfo", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds = _cn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_GET_ASN_INFO", cmdParameters)

            Return ds
        Catch ex As Exception
            Throw ex
            'Throw New Exception(_strSql)
        End Try
    End Function
    Public Function GetWaveShipmentDetails(ByVal searchParams As WaveSearchParams) As DataSet
        Try
            Dim ds, bsDs As DataSet
            If _connStr = "" Then
                SetWhseDBConnectionByID(searchParams.whse)
            End If
            _cn = New OracleDataManager(_connStr)
            '_strSql = BuildWaveToolSql(searchParams)
            Dim cmdParameters() As OracleParameter = New OracleParameter(7) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = searchParams.ShipmentNbr
            cmdParameters(1) = New OracleParameter("DistroDtl", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            cmdParameters(2) = New OracleParameter("DistroSummary", OracleType.Cursor)
            cmdParameters(2).Direction = ParameterDirection.Output
            cmdParameters(3) = New OracleParameter("PrevWave", OracleType.Cursor)
            cmdParameters(3).Direction = ParameterDirection.Output
            cmdParameters(4) = New OracleParameter("CaseSummary", OracleType.Cursor)
            cmdParameters(4).Direction = ParameterDirection.Output
            cmdParameters(5) = New OracleParameter("CaseLock", OracleType.Cursor)
            cmdParameters(5).Direction = ParameterDirection.Output
            cmdParameters(6) = New OracleParameter("ResvLocType", OracleType.Cursor)
            cmdParameters(6).Direction = ParameterDirection.Output
            cmdParameters(7) = New OracleParameter("MultiSkuCase", OracleType.Cursor)
            cmdParameters(7).Direction = ParameterDirection.Output

            ds = _cn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_MAIN", cmdParameters)
            'ds = _cn.ExecuteDataSet(_strSql)
            'Get back stock units

            'Modified by Hogan 8-3-2011 to remove pointing to Arthur for Backstock units.
            'If (searchParams.whse = "997") Then
            '    bsDs = GetBackStockUnits(searchParams)
            'Else
            '    bsDs = GetWMSBackStockUnits(searchParams)
            'End If

            bsDs = GetWMSBackStockUnits(searchParams)

            If bsDs.Tables.Count > 0 AndAlso bsDs.Tables(0).Rows.Count > 0 Then
                bsDs.Tables(0).TableName = "BackStock"
                If ds.Tables.Count > 0 Then
                    ds.Tables.Add(bsDs.Tables(0).Copy)
                End If
            End If
            Return ds
        Catch ex As Exception
            Throw ex
            'Throw New Exception(_strSql)
        End Try
    End Function
    Private Function GetBackStockUnits(ByVal searchParams As WaveSearchParams) As DataSet
        Dim bsDs As DataSet
        _cn = New OracleDataManager(_aaConnStr)
        Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
        cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
        cmdParameters(0).Value = searchParams.ShipmentNbr
        cmdParameters(1) = New OracleParameter("p_whse_nbr", OracleType.VarChar, 3)
        cmdParameters(1).Value = searchParams.whse
        cmdParameters(2) = New OracleParameter("BackStock", OracleType.Cursor)
        cmdParameters(2).Direction = ParameterDirection.Output
        bsDs = _cn.ExecuteDataSet("HT_WAVE_TOOL.PROC_GET_BACKSTOCK_UNITS", cmdParameters)
        Return bsDs
    End Function

    Private Function GetWMSBackStockUnits(ByVal searchParams As WaveSearchParams) As DataSet
        Try
            Dim bsDs As DataSet

            If _connStr = "" Then
                SetWhseDBConnectionByID(searchParams.whse)
            End If
            _cn = New OracleDataManager(_connStr)

            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = searchParams.ShipmentNbr
            cmdParameters(1) = New OracleParameter("BackStock", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            bsDs = _cn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_GET_BACKSTOCK_UNITS", cmdParameters)
            Return bsDs
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function AdjustDistroUnits(ByVal ShipmentNbr As String, ByVal DistroNbr As String, ByVal SkuID As String, ByVal AllocUnits As Int16, ByVal AdjustQty As Int16) As Int16
        Try
            _cn = New OracleDataManager(_connStr)

            Dim cmdParameters() As OracleParameter = New OracleParameter(4) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = ShipmentNbr
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 20)
            cmdParameters(1).Value = DistroNbr
            cmdParameters(2) = New OracleParameter("p_sku_ID", OracleType.VarChar, 20)
            cmdParameters(2).Value = SkuID
            cmdParameters(3) = New OracleParameter("p_alloc_units", OracleType.Int16)
            cmdParameters(3).Value = AllocUnits
            cmdParameters(3).Direction = ParameterDirection.InputOutput
            cmdParameters(4) = New OracleParameter("p_adjust_qty", OracleType.Int16)
            cmdParameters(4).Value = AdjustQty
            _cn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_ADJUSTDISTRO", cmdParameters)
            Return cmdParameters(3).Value
        Catch ex As Exception
            Throw ex
            'Throw New Exception(_strSql)
        End Try
    End Function
    Public Function UpdateStoreDistro(ByVal ShipmentNbr As String, ByVal DistroNbr As String, ByVal SkuID As String, ByVal ItemNbr As String)
        Dim cmdParameters() As OracleParameter = New OracleParameter(3) {}
        cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
        cmdParameters(0).Value = ShipmentNbr
        cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 20)
        cmdParameters(1).Value = DistroNbr
        cmdParameters(2) = New OracleParameter("p_sku_ID", OracleType.VarChar, 20)
        cmdParameters(2).Value = SkuID
        cmdParameters(3) = New OracleParameter("p_item_nbr", OracleType.VarChar, 20)
        cmdParameters(3).Value = ItemNbr
        _cn = New OracleDataManager(_connStr)

        Try
            'flLogFile.WriteItem("ShipmentNbr = " & ShipmentNbr & ", DistroNbr = " & DistroNbr & ", SkuID =" & SkuID)
            _cn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_UPDATE_DISTRO", cmdParameters)
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    'Public Function UpdateItemReceiveTemp(ByVal ShipmentNbr As String, ByVal DistroNbr As String, ByVal itemNbr As String, ByVal SkuID As String, ByVal RecvQty As Int16)
    '    Dim cmdParameters() As OracleParameter = New OracleParameter(4) {}
    '    cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
    '    cmdParameters(0).Value = ShipmentNbr
    '    cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 20)
    '    cmdParameters(1).Value = DistroNbr
    '    cmdParameters(2) = New OracleParameter("p_item_nbr", OracleType.VarChar, 20)
    '    cmdParameters(2).Value = itemNbr
    '    cmdParameters(3) = New OracleParameter("p_sku_ID", OracleType.VarChar, 20)
    '    cmdParameters(3).Value = SkuID
    '    cmdParameters(4) = New OracleParameter("p_Recv_Qty", OracleType.Int16)
    '    cmdParameters(4).Value = RecvQty
    '    _cn = New OracleDataManager(_connStr)
    '    Try
    '        flLogFile.WriteItem("ShipmentNbr = " & ShipmentNbr & ", DistroNbr = " & DistroNbr & ", SkuID =" & SkuID & ", RecvQty = " & RecvQty)
    '        _cn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_UPDATE_ITEM_RECEIVE_TEMP", cmdParameters)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Private Function BuildWaveToolSql(ByVal searchParams As WaveSearchParams) As String
    '    Dim strSql, sqlFilter As String
    '    strSql = ""
    '    sqlFilter = ""


    '    strSql = "SELECT STYLE ||'-' || STYLE_SFX  as SKU_NUM, SKU_DESC,  A.UNITS_SHPD,A.UNITS_RCVD" & _
    '            " ,NVL(SD.DISTRO_NBR,0) AS DISTRO_NBR,NVL(SUM(REQD_QTY),0) AS ALLOC_UNITS, NVL(SUM(WAVE_ALLOC_QTY) ,0) AS WAVE_UNITS " & _
    '            " ,nvl(I.MERCH_TYPE,' ') AS ITEM_MERCH_TYPE,nvl(SD.MERCH_TYPE,' ') AS DISTRO_MERCH_TYPE " & _
    '            " FROM %DbSchema%ASN_DTL A JOIN %DbSchema%item_master I  ON A.SKU_ID = I.SKU_ID" & _
    '            " LEFT OUTER JOIN %DbSchema%STORE_DISTRO SD ON A.SHPMT_NBR = SD.SHPMT_NBR AND A.SKU_ID = SD.SKU_ID " & _
    '            " WHERE  A.SHPMT_NBR = '" & searchParams.ShipmentNbr & "'" & _
    '            " GROUP BY  STYLE ||'-' || STYLE_SFX, SKU_DESC,DISTRO_NBR,A.UNITS_SHPD,A.UNITS_RCVD,I.MERCH_TYPE,SD.MERCH_TYPE" & _
    '            " ORDER BY SKU_NUM,DISTRO_NBR"
    '    'Distro Summary SQL
    '    strSql &= ";SELECT CODE_DESC,count(SD.STAT_CODE) AS CNT,sum(SD.REQD_QTY) AS ALLOC_UNITS" & _
    '            " FROM %DbSchema%STORE_DISTRO SD JOIN %DbSchema%SYS_CODE S ON SD.STAT_CODE = S.CODE_ID " & _
    '            " WHERE SD.STAT_CODE <> 95 AND S.CODE_TYPE = '791' AND S.REC_TYPE = 'S'" & _
    '            " AND SD.SHPMT_NBR = '" & searchParams.ShipmentNbr & "'" & _
    '            " GROUP BY CODE_DESC"
    '    ''searchParams.ShipmentNbr = IIf(searchParams.ShipmentNbr.Trim <> "", "%", "") & searchParams.ShipmentNbr
    '    'SqlQueryBuilder(sqlFilter, "SHPMT_NBR", searchParams.ShipmentNbr, SqlDbType.VarChar)
    '    'strSql &= sqlFilter & " ORDER BY PO_NBR"
    '    strSql = strSql.Replace("%DbSchema%", _dbSchema)
    '    Return strSql
    'End Function
    Private Shared Sub SqlQueryBuilder(ByRef sqlFilter As String, ByVal colName As String, ByVal colVal As String, ByVal dataType As SqlDbType, Optional ByVal CompareMode As String = "=")
        If Not IsNothing(colVal) AndAlso colVal <> "" Then
            sqlFilter &= IIf(sqlFilter.Length > 0, " AND ", " WHERE ")
            If UCase(CompareMode) = "IN" Then
                sqlFilter &= colName & " IN (" & colVal & ") "
            Else
                CompareMode = " " & CompareMode & " "
                Select Case dataType
                    Case SqlDbType.Int, SqlDbType.BigInt, SqlDbType.Binary, SqlDbType.Float, SqlDbType.Money, SqlDbType.Decimal, SqlDbType.SmallInt, SqlDbType.SmallMoney, SqlDbType.Bit
                        sqlFilter &= colName & CompareMode & colVal
                    Case SqlDbType.DateTime, SqlDbType.SmallDateTime
                        sqlFilter &= colName & CompareMode & "'" & colVal & "'"
                    Case SqlDbType.VarChar, SqlDbType.NChar, SqlDbType.NVarChar, SqlDbType.Char, SqlDbType.Text, SqlDbType.NText
                        sqlFilter &= colName & CompareMode & "'" & UtilityManager.InsertApos(colVal) & "'"
                    Case Else
                        sqlFilter &= colName & CompareMode & UtilityManager.InsertApos(colVal) & ""
                End Select
            End If
        End If
    End Sub
    Private Shared Sub SqlQueryBuilderByDate(ByRef sqlFilter As String, ByVal colName As String, ByVal dateFrom As String, ByVal dateTo As String)
        If IsDate(dateFrom) OrElse IsDate(dateTo) Then
            If Len(sqlFilter) > 0 Then
                sqlFilter &= " AND "
            Else
                sqlFilter &= " WHERE "
            End If
            If IsDate(dateFrom) AndAlso IsDate(dateTo) Then
                If CDate(dateTo) > CDate(dateFrom) Then
                    sqlFilter &= colName & " BETWEEN '" & dateFrom & "' AND '" & dateTo & "'"
                Else
                    sqlFilter &= colName & " BETWEEN '" & dateTo & "' AND '" & dateFrom & "'"
                End If
            ElseIf IsDate(dateFrom) Then
                sqlFilter &= colName & " >= '" & dateFrom & "'"
            ElseIf IsDate(dateTo) Then
                sqlFilter &= colName & " <= '" & dateTo & "'"
            End If
        End If
    End Sub
End Class
<Serializable()> _
Public Structure WaveSearchParams
    Public PoNbr As String
    Public ShipmentNbr As String
    Public whse As Int16
End Structure
