Public Class WaveByToolSearch
    Inherits System.Web.UI.Page
    Protected WithEvents lstWhse As System.Web.UI.WebControls.DropDownList
    Protected WithEvents reqValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtPoNbr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtShipmentNbr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents ValidationSummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents validateShipmentNbr As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents validatePonum As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents dgResults As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Private _searchParams As WaveSearchParams
    Private _isValidRequest As Boolean = False
    Private _ds As DataSet
    Protected WithEvents lblRecCount As System.Web.UI.WebControls.Label
    Protected WithEvents pnlResults As System.Web.UI.WebControls.Panel
    Protected WithEvents hdnCheckedItemStr As System.Web.UI.HtmlControls.HtmlInputHidden
    Private rptManager As ReportManager
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblError.Visible = False
        lblError.Text = ""
        Try
            If Not IsPostBack Then
                InitializeForm()
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub
    Private Sub InitializeForm()
        If Request("whse") <> "" Then
            Dim li As ListItem
            lstWhse.SelectedIndex = -1
            For Each li In lstWhse.Items
                If li.Value = Request("whse") Then
                    li.Selected = True
                    lstWhse.Enabled = False
                    Exit For
                End If
            Next
        End If
    End Sub
    Private Sub PrepareSearchParams()
        If txtPoNbr.Text.Trim <> "" Then
            _searchParams.PoNbr = txtPoNbr.Text.Trim
            _isValidRequest = True
        End If
        If txtShipmentNbr.Text.Trim <> "" Then
            _searchParams.ShipmentNbr = txtShipmentNbr.Text.Trim
            _isValidRequest = True
        End If
        _searchParams.whse = lstWhse.SelectedItem.Value
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            PrepareSearchParams()
            If _isValidRequest Then
                BindData()
            Else
                ShowMessage("Please fill atleast one search parameter!")
            End If
        Catch ex As Exception
            ShowMessage(ex.ToString)
        End Try
    End Sub
    Private Sub BindData()
        rptManager = New ReportManager(_searchParams.whse)
        '_ds = rptManager.listWaveShipmentNbr(_searchParams)
        'If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
        '    dgResults.DataSource = _ds.Tables(0)
        '    '_ds.Tables(0).DefaultView.Sort = SortBy.Text & " " & SortDirection.Text
        '    If dgResults.CurrentPageIndex > 0 AndAlso _ds.Tables(0).Rows.Count <= (dgResults.CurrentPageIndex * dgResults.PageSize) Then
        '        dgResults.CurrentPageIndex = dgResults.CurrentPageIndex - 1
        '    End If
        '    dgResults.DataBind()
        '    lblRecCount.Text = "[ " & _ds.Tables(0).Rows.Count & " Record(s) found. ]"
        '    dgResults.Visible = True
        'Else
        '    lblRecCount.Text = "[ 0 Record(s) found. ]"
        '    dgResults.Visible = False
        'End If
    End Sub
    Private Sub ShowMessage(ByVal msg As String)
        lblError.Visible = True
        lblError.Text = msg
    End Sub
End Class
