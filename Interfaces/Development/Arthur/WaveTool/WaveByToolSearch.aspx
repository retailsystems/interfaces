<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WaveByToolSearch.aspx.vb" Inherits="WMReports.WaveByToolSearch"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<!-- #include file="include/header.inc" -->
	<body>
		<form id="Form1" method="post" runat="server">
			<table border="0" width="800" align="left">
				<TR height="3">
					<td></td>
				</TR>
				<TR>
					<td align="center" class="pagecaption">Wave by Tool Report</td>
				</TR>
				<TR height="3">
					<td></td>
				</TR>
				<tr>
					<td valign="top">
						<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
							cellSpacing="0" cellPadding="2" width="98%" align="center" border="0">
							<TR height="15">
								<TD class="cellvaluecaption" bgColor="#990000" align="center">Search Criteria
								</TD>
							</TR>
							<TR height="5">
								<TD></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="2">
									<TABLE borderColor="#990000" cellSpacing="0" cellPadding="2" width="100%" border="0">
										<TR height="3">
											<td></td>
											<TD colSpan="4">
												<asp:Label Runat="server" ID="lblError" CssClass="errStyle" Visible="False"></asp:Label>
												<asp:ValidationSummary Runat="server" EnableClientScript="True" DisplayMode="BulletList" CssClass="errStyle"
													ShowMessageBox="False" id="ValidationSummary1"></asp:ValidationSummary>
											</TD>
											<td></td>
										</TR>
										<tr>
											<td></td>
											<td class="cellvaluecaption">
												Warehouse:
												<asp:DropDownList id="lstWhse" Runat="server" CssClass="cellvalueleft">
													<asp:ListItem Value="">--select one--</asp:ListItem>
													<asp:ListItem Value="999">DC-Industry,CA</asp:ListItem>
													<asp:ListItem Value="997">DC-LaVergne,TN</asp:ListItem>
												</asp:DropDownList>
												<asp:RequiredFieldValidator Runat="server" ID="reqValidator1" CssClass="reqStyle" EnableClientScript="true"
													ControlToValidate="lstWhse" Display="None" ErrorMessage="Please select a Warehouse." InitialValue=""></asp:RequiredFieldValidator>
											</td>
											<td class="cellvaluecaption">
												PO #:
												<asp:TextBox ID="txtPoNbr" Width="100px" CssClass="cellvalueleft" Runat="server" MaxLength="6"></asp:TextBox>
												<asp:CompareValidator ID="validatePonum" Display="None" Runat="server" ErrorMessage="Invalid PO #." Type="Integer"
													Operator="DataTypeCheck" EnableClientScript="true" ControlToValidate="txtPoNbr"></asp:CompareValidator>
											</td>
											<td class="cellvaluecaption">
												Shipment #:
												<asp:TextBox ID="txtShipmentNbr" CssClass="cellvalueleft" Width="150px" MaxLength="20" Runat="server"></asp:TextBox>
												<asp:CompareValidator ID="validateShipmentNbr" Display="None" Runat="server" ErrorMessage="Invalid Shipment #."
													Type="Integer" Operator="DataTypeCheck" EnableClientScript="true" ControlToValidate="txtShipmentNbr"></asp:CompareValidator>
											</td>
											<td align="right">
												<asp:Button Runat="server" ID="btnSubmit" CssClass="btnSmall" Text="Submit"></asp:Button>
												&nbsp;<input type="button" class="btnSmall" name="btnClear" id="btnClear" value="Clear" onclick="location.href=location.href">
											</td>
										</tr>
										<tr>
											<td colspan="4" align="center">
											</td>
										</tr>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<!-- Search Results -->
				<asp:Panel Runat="server" ID="pnlResults" Visible="True" EnableViewState="False">
					<TR>
						<TD vAlign="top">
							<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
								cellSpacing="0" cellPadding="2" width="98%" align="center" border="0">
								<TR height="15">
									<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Results
									</TD>
								</TR>
								<TR height="5">
									<TD></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center" colSpan="2">
										<TABLE borderColor="#990000" cellSpacing="0" cellPadding="2" width="100%" border="0">
											<TR height="3">
												<TD width="10"></TD>
												<TD>
													<asp:Label id="lblRecCount" CssClass="ErrStyle" Runat="server"></asp:Label><INPUT id="hdnCheckedItemStr" type="hidden" name="hdnCheckedItemStr" runat="server">
												</TD>
											</TR>
											<TR>
												<TD width="10"></TD>
												<TD align="center"><!-- Search Results -->
													<asp:DataGrid id="dgResults" Runat="server" AutoGenerateColumns="False">
														<Columns>
															<asp:BoundColumn DataField="SHPMT_NBR" HeaderText="Shipment #"></asp:BoundColumn>
															<asp:BoundColumn DataField="PO_NBR" HeaderText="PO #"></asp:BoundColumn>
															<asp:TemplateColumn>
																<ItemTemplate>
																	<a href="javascript:ShowReport()">Details</a>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:DataGrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</asp:Panel>
			</table>
		</form>
		</TD></TR>
		<TR vAlign="top">
			<TD width="0" height="995"></TD>
			<TD>
				<!-- #include file="include/footer.inc" --></TD>
		</TR>
		</TABLE>
	</body>
</HTML>
