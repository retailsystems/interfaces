Imports HotTopic.DCSS.Services
Imports HotTopic.AIMS.Logging
Public Class WaveByToolReport
    Inherits System.Web.UI.Page
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents ValidationSummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents txtShipmentNbr As System.Web.UI.WebControls.TextBox
    Protected WithEvents validateShipmentNbr As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents lstWhse As System.Web.UI.WebControls.DropDownList
    Protected WithEvents reqValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Private _searchParams As WaveSearchParams
    Private _isValidRequest As Boolean = False
    Protected _ds, _dsDetails As DataSet
    Protected _dr As DataRow
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lblShpmtNbr As System.Web.UI.WebControls.Label
    Protected WithEvents lblPoNbr As System.Web.UI.WebControls.Label
    Protected WithEvents lblShpmtStatus As System.Web.UI.WebControls.Label
    Protected WithEvents lblFirstRcptDt As System.Web.UI.WebControls.Label
    Protected WithEvents pnlShpmtHdr As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlShpmtDtl As System.Web.UI.WebControls.Panel
    Protected WithEvents dgDistroSummary As System.Web.UI.WebControls.DataGrid
    Private rptManager As ReportManager
    Dim totCnt, totAlloc As Int32
    Public fileName, code_desc As String
    Public flLogFile As FileLogger
    Protected WithEvents lblPrevWave As System.Web.UI.WebControls.Label
    Protected WithEvents dgCaseSummary As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblCaseLock As System.Web.UI.WebControls.Label
    Protected WithEvents dgCaseLock As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgResvLocType As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblMultiSkuCase As System.Web.UI.WebControls.Label
    Protected WithEvents btnAdjust As System.Web.UI.WebControls.Button
    Protected WithEvents hdnDistroParams As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnAdjustWL As System.Web.UI.WebControls.Button
    Protected WithEvents dgPrevWave As System.Web.UI.WebControls.DataGrid
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        fileName = Server.MapPath("~/") & "\" & DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") & ".log"
        flLogFile = New FileLogger(fileName)
        lblError.Visible = False
        lblError.Text = ""
        Try
            If Not IsPostBack Then
                InitializeForm()
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub
    Private Sub InitializeForm()

        If Request("whse") <> "" Then
            Dim li As ListItem
            lstWhse.SelectedIndex = -1
            For Each li In lstWhse.Items
                If li.Value = Request("whse") Then
                    li.Selected = True
                    lstWhse.Visible = False
                    reqValidator1.Enabled = False
                    Exit For
                End If
            Next
        End If
        btnSubmit.Attributes.Add("onClick", "showWait();")
        'Enabling Enter key events
        'Page.RegisterHiddenField("__EVENTTARGET", btnSubmit.ClientID)
        txtShipmentNbr.Attributes.Add("onkeydown", "return isEnterkeyPressed();")
    End Sub
    Private Sub BindData()
        rptManager = New ReportManager(_searchParams.whse)
        _ds = rptManager.GetShipmentHdrInfo(_searchParams)
        If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
            _dr = _ds.Tables(0).Rows(0)
            lblShpmtNbr.Text = _dr("SHPMT_NBR")
            lblPoNbr.Text = UtilityManager.NullToString(_dr("PO_NBR"))
            lblShpmtStatus.Text = UtilityManager.NullToString(_dr("CODE_DESC"))
            lblFirstRcptDt.Text = UtilityManager.NullToString(_dr("FIRST_RCPT_DATE_TIME"))
            pnlShpmtHdr.Visible = True
            _dsDetails = rptManager.GetWaveShipmentDetails(_searchParams)
            If _dsDetails.Tables.Count > 0 AndAlso _dsDetails.Tables(0).Rows.Count > 0 Then
                pnlShpmtDtl.Visible = True
                'Show Distro summary
                If _dsDetails.Tables(1).Rows.Count > 0 Then
                    dgDistroSummary.DataSource = _dsDetails.Tables(1)
                    dgDistroSummary.DataBind()
                End If
                'Show Prewave
                If _dsDetails.Tables(2).Rows.Count > 0 Then
                    lblPrevWave.Text = "YES"
                    dgPrevWave.DataSource = _dsDetails.Tables(2)
                    dgPrevWave.DataBind()
                Else
                    lblPrevWave.Text = "NO"
                End If
                'Show Case summary
                If _dsDetails.Tables(3).Rows.Count > 0 Then
                    dgCaseSummary.DataSource = _dsDetails.Tables(3)
                    dgCaseSummary.DataBind()
                End If
                ' Show Case Lock
                If _dsDetails.Tables(4).Rows.Count > 0 Then
                    lblCaseLock.Text = "YES"
                    dgCaseLock.DataSource = _dsDetails.Tables(4)
                    dgCaseLock.DataBind()
                Else
                    lblCaseLock.Text = "NO"
                End If
                ' Reserver loc type
                If _dsDetails.Tables(5).Rows.Count > 0 Then
                    dgResvLocType.DataSource = _dsDetails.Tables(5)
                    dgResvLocType.DataBind()
                End If
                ' Multi Sku case
                If _dsDetails.Tables(6).Rows.Count > 0 Then
                    lblMultiSkuCase.Text = "YES"
                Else
                    lblMultiSkuCase.Text = "NO"
                End If
                'ShowMessage("# table: " & _dsDetails.Tables("BackStock").Rows.Count)
            Else
                pnlShpmtDtl.Visible = False
            End If
        Else
            pnlShpmtHdr.Visible = False
            ShowMessage("Shipment information not available")
        End If
    End Sub
    Private Sub ShowMessage(ByVal msg As String)
        lblError.Visible = True
        lblError.Text = msg
    End Sub
    Private Sub PrepareSearchParams()
        If txtShipmentNbr.Text.Trim <> "" Then
            _searchParams.ShipmentNbr = txtShipmentNbr.Text.Trim
            _isValidRequest = True
        End If
        _searchParams.whse = lstWhse.SelectedItem.Value
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            PrepareSearchParams()
            If _isValidRequest Then
                BindData()
            Else
                ShowMessage("Please fill atleast one search parameter!")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub dgDistroSummary_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDistroSummary.ItemDataBound

        Select Case e.Item.ItemType
            Case ListItemType.Header
                totCnt = 0
                totAlloc = 0
            Case ListItemType.Item, ListItemType.AlternatingItem
                totCnt += CInt("0" + e.Item.Cells(2).Text)
                totAlloc += CInt("0" + e.Item.Cells(3).Text)
            Case ListItemType.Footer
                e.Item.Cells(1).Text = "Total "
                e.Item.Cells(1).Attributes.Add("align", "right")
                e.Item.Cells(2).Text = totCnt.ToString
                e.Item.Cells(3).Text = totAlloc.ToString
        End Select
    End Sub

    Private Sub dgCaseSummary_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCaseSummary.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header
                totCnt = 0
            Case ListItemType.Item, ListItemType.AlternatingItem
                totCnt += CInt("0" + e.Item.Cells(2).Text)
            Case ListItemType.Footer
                e.Item.Cells(1).Text = "Total "
                e.Item.Cells(1).Attributes.Add("align", "right")
                e.Item.Cells(2).Text = totCnt.ToString
        End Select
    End Sub

    Private Sub dgCaseLock_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCaseLock.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header
                totCnt = 0
            Case ListItemType.Item, ListItemType.AlternatingItem
                totCnt += CInt("0" + e.Item.Cells(1).Text)
            Case ListItemType.Footer
                e.Item.Cells(0).Text = "Total "
                e.Item.Cells(0).Attributes.Add("align", "right")
                e.Item.Cells(1).Text = totCnt.ToString
        End Select
    End Sub
    Public Function GetBackStockUnits(ByVal SKU_NBR As String, ByVal distro_nbr As String) As Int32
        Try
            Dim bs_qty As Int32 = 0
            Dim alloc_nbr As Int32 = CInt("0" + distro_nbr.Trim)
            Dim dr As DataRow

            If _dsDetails.Tables.Contains("BackStock") AndAlso _dsDetails.Tables("BackStock").Rows.Count > 0 Then
                For Each dr In _dsDetails.Tables("BackStock").Rows
                    If alloc_nbr > 0 Then
                        If dr("ALLOCATION_NBR") = alloc_nbr AndAlso dr("SKU_NBR") = SKU_NBR Then
                            bs_qty = dr("RESULT_QTY")
                            Exit For
                        End If
                    Else
                        If dr("SKU_NBR") = SKU_NBR Then
                            bs_qty = dr("RESULT_QTY")
                            Exit For
                        End If
                    End If
                Next
            End If

            Return bs_qty
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Function
    Private Sub btnAdjustWL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdjustWL.Click

        Dim ShipmentNbr, DistroNbr, SkuID, itemNbr As String
        Dim AllocUnits, RecQty, NewAllocUnits, AdjustQty, MaxIterations, BalanceQty, i As Int16
        ShipmentNbr = lblShpmtNbr.Text
        Try
            'parse hdnDistroParams (DistroNbr|SkuID|AllocUnits|AdjustQty)
            Dim DistroParamsArr As String() = hdnDistroParams.Value.Split("|")
            DistroNbr = DistroParamsArr(0)
            SkuID = DistroParamsArr(1)
            RecQty = DistroParamsArr(2)
            itemNbr = DistroParamsArr(3)
            'ShowMessage(ShipmentNbr & "|" & DistroNbr & "|" & SkuID & "|" & AllocUnits & "|" & AdjustQty)
            rptManager = New ReportManager(lstWhse.SelectedItem.Value)
            rptManager.UpdateStoreDistro(ShipmentNbr, DistroNbr, SkuID, itemNbr)

            'SELECT PO_NBR, ASN, ALLOC_NBR, ITM_NBR, MERCH_ID_TYPE FROM WORKLIST 
            '   WHERE ALLOC_NBR = '540391' AND ITM_NBR = '325125' 
            '   AND STATUS_CODE = 40 
            '   AND PO_NBR <> 131457 
            ' AND ASN <> '00000000000000201379' ;

            'reload wave report data
            hdnDistroParams.Value = ""
            txtShipmentNbr.Text = ShipmentNbr
            PrepareSearchParams()
            If _isValidRequest Then
                BindData()
                ShowMessage("Item " & itemNbr & " is adjusted!")
            Else
                ShowMessage("Please fill atleast one search parameter!")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        Finally
            hdnDistroParams.Value = ""
        End Try
    End Sub

    Private Sub btnAdjust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdjust.Click
        Try
            'ShowMessage(hdnDistroParams.Value)
            Dim ShipmentNbr, DistroNbr, SkuID As String
            Dim AllocUnits, NewAllocUnits, AdjustQty, MaxIterations, BalanceQty, i As Int16
            ShipmentNbr = lblShpmtNbr.Text
            'parse hdnDistroParams (DistroNbr|SkuID|AllocUnits|AdjustQty)
            Dim DistroParamsArr As String() = hdnDistroParams.Value.Split("|")
            DistroNbr = DistroParamsArr(0)
            SkuID = DistroParamsArr(1)
            AllocUnits = DistroParamsArr(2)
            AdjustQty = DistroParamsArr(3)
            'ShowMessage(ShipmentNbr & "|" & DistroNbr & "|" & SkuID & "|" & AllocUnits & "|" & AdjustQty)
            rptManager = New ReportManager(lstWhse.SelectedItem.Value)


            If Not IsNothing(Application("DistroAdj_IterationMax")) AndAlso IsNumeric(Application("DistroAdj_IterationMax")) Then
                MaxIterations = Application("DistroAdj_IterationMax")
            End If
            BalanceQty = 0
            MaxIterations = IIf(MaxIterations > 0, MaxIterations, 1)
            'call AdjustDistroUnits
            For i = 1 To MaxIterations
                NewAllocUnits = rptManager.AdjustDistroUnits(ShipmentNbr, DistroNbr, SkuID, AllocUnits, AdjustQty)
                If NewAllocUnits <= 0 OrElse NewAllocUnits = AllocUnits + AdjustQty Then
                    Exit For
                Else
                    'call AdjustDistroUnits again
                    AdjustQty = AllocUnits + AdjustQty - NewAllocUnits
                    AllocUnits = NewAllocUnits
                End If
            Next
            'ShowMessage(NewAllocUnits & ":" & MaxIterations & i & AdjustQty)
            'reload wave report data
            hdnDistroParams.Value = ""
            txtShipmentNbr.Text = ShipmentNbr
            PrepareSearchParams()
            If _isValidRequest Then
                BindData()
            Else
                ShowMessage("Please fill atleast one search parameter!")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        Finally
            hdnDistroParams.Value = ""
        End Try
    End Sub
End Class
