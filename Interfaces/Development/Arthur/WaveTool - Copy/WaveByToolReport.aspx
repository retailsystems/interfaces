<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WaveByToolReport.aspx.vb" Inherits="WMReports.WaveByToolReport"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WaveByToolReport</title>
		<META HTTP-EQUIV="refresh" content="900">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			function isEnterkeyPressed(){
				if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))
					{document.Form1.btnSubmit.click();return false;} 
				else return true;
			}
			function fnAllocAdjust(distroParamStr){
				if (confirm("Are you sure to adjust the allocations?")){
					document.Form1.hdnDistroParams.value = distroParamStr;
					//alert(document.Form1.hdnDistroParams.value);
					document.Form1.btnAdjust.click();
				}
			}
			function fnAllocAdjustWL(distroParamStr){
				if (confirm("Are you sure to adjust the allocations?")){
					document.Form1.hdnDistroParams.value = distroParamStr;
					//alert(document.Form1.hdnDistroParams.value);
					document.Form1.btnAdjustWL.click();
				}
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<input type="hidden" runat="server" id="hdnDistroParams" name="hdnDistroParams">
			<asp:Button Runat="server" ID="btnAdjust" Text="" Width="0px" CausesValidation="False"></asp:Button>
			<asp:Button Runat="server" ID="btnAdjustWL" Text="" Width="0px" CausesValidation="False"></asp:Button>
			<table width="800">
				<tr>
					<td height="3"></td>
				</tr>
				<TR>
					<td>
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td width="200"><IMG src="images/hdr_main2.gif"></td>
								<td class="pagecaption" vAlign="bottom" align="center">Wave Tool Report</td>
								<td align="right" width="200"><IMG src="images/Torridblk.gif"></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td height="3"></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table2" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid" height="80" cellSpacing="0" cellPadding="2" width="98%" align="center" border="0">
							<TR height="3">
								<td width="10"></td>
								<TD colSpan="2"><asp:label id="lblError" Runat="server" CssClass="reqStyle" Visible="False"></asp:label><asp:validationsummary id="ValidationSummary1" Runat="server" CssClass="errStyle" EnableClientScript="True" DisplayMode="BulletList" ShowMessageBox="False"></asp:validationsummary></TD>
								<td width="10"></td>
							</TR>
							<tr>
								<td width="10"></td>
								<td class="cellvaluecaption" colSpan="2"><asp:dropdownlist id="lstWhse" Runat="server" CssClass="cellvalueleft">
										<asp:ListItem Value="">--Select Warehouse--</asp:ListItem>
										<asp:ListItem Value="999">DC-Industry,CA</asp:ListItem>
										<asp:ListItem Value="997">DC-LaVergne,TN</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="reqValidator1" Runat="server" CssClass="reqStyle" EnableClientScript="true" ControlToValidate="lstWhse" Display="None" ErrorMessage="Please select a Warehouse." InitialValue=""></asp:requiredfieldvalidator>Shipment 
									#:
									<asp:textbox id="txtShipmentNbr" Runat="server" CssClass="cellvalueleft" Width="200px" MaxLength="20"></asp:textbox><asp:comparevalidator id="validateShipmentNbr" Runat="server" EnableClientScript="true" ControlToValidate="txtShipmentNbr" Display="None" ErrorMessage="Invalid Shipment #." Type="Double" Operator="DataTypeCheck"></asp:comparevalidator><asp:requiredfieldvalidator id="Requiredfieldvalidator1" Runat="server" CssClass="reqStyle" EnableClientScript="true" ControlToValidate="txtShipmentNbr" Display="None" ErrorMessage="Please enter Shipment #." InitialValue=""></asp:requiredfieldvalidator>&nbsp;&nbsp;<asp:button id="btnSubmit" Runat="server" CssClass="btnSmallGrey" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="btnSmallGrey" id="btnClear" onclick="location.href=location.href" type="button" value="Clear" name="btnClear">
								</td>
								<td width="10"></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td colSpan="2"><asp:panel id="pnlShpmtHdr" Runat="server" Visible="False"><FONT class="cellValueCaption">Shipment 
											#:</FONT>
										<asp:Label id="lblShpmtNbr" Runat="server" CssClass="cellValueLeft" EnableViewState="true"></asp:Label>
										<IMG height="1" src="images/wspacer.gif" width="10"> <FONT class="cellValueCaption">
											PO #:</FONT>
										<asp:Label id="lblPoNbr" Runat="server" CssClass="cellValueLeft" EnableViewState="False"></asp:Label>
										<IMG height="1" src="images/wspacer.gif" width="10"> <FONT class="cellValueCaption">
											Status:</FONT>
										<asp:Label id="lblShpmtStatus" Runat="server" CssClass="cellValueLeft" EnableViewState="False"></asp:Label>
										<IMG height="1" src="images/wspacer.gif" width="10"> <FONT class="cellValueCaption">
											First Receipt Date:</FONT>
										<asp:Label id="lblFirstRcptDt" Runat="server" CssClass="cellValueLeft" EnableViewState="False"></asp:Label>
										<IMG height="1" src="images/wspacer.gif" width="5">
									</asp:panel></td>
								<td width="10"></td>
							</tr>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><br>
						<!-- #include file="include/wait.inc" -->
					</td>
				</tr>
				<asp:panel id="pnlShpmtDtl" Runat="server" Visible="False" EnableViewState="False">
					<TR>
						<TD><%
						TRY
						IF _dsDetails.Tables(0).Rows.Count > 0 THEN
							Dim prevSku as string = ""
							Dim unitsOrdered, unitsShpd,unitsRcvd,unitsBs, rcptVar, rcptVarPct, totRcptVar, totRcptVarPct, itemNbr as string
							Dim totUnitsOrdered, totShpd,totRcvd,totAlloc,totBs,totWave,allocVar,totAllocVar, unitAlloc, adjustQty as int32
							Dim duplicateFlag as boolean = false
							'01/17/2006 -Sri Bajjuri - Alloc Adjustment enhancements							
							Dim  allocVarPct as double = 0
							Dim allocAdjustUrl as string = ""
							Dim allocAdjustLimit as int32 = Application("Alloc_Adjust_Limit")
							
							%>
							<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD colSpan="16">
										<HR style="COLOR: black; HEIGHT: 1px">
									</TD>
								</TR>
								<TR>
									<TH align="left" width="70">
										DISTRO #</TH>
									<TH align="left" width="70">
										SKU</TH><!--
									<TH align="left" width="140">
										ALLOC VAR(%))</TH>
									-->
									<TH align="center" width="50">
										PO UNITS</TH>
									<TH align="center" width="50">
										ASN UNITS</TH>
									<TH align="center" width="50">
										RCVD UNITS</TH>
									<TH align="center" width="50">
										ALLOC UNITS</TH>
									<TH align="center" width="60">
										BACKSTOCK UNITS</TH>
									<TH align="center" width="60">
										RCPT VAR UNITS</TH>
									<TH align="center" width="60">
										RCPT VAR PCT</TH>
									<TH align="center" width="50">
										ALLOC VAR</TH>
									<TH align="center" width="50">
										WAVE UNITS</TH>
									<TH align="center" width="50">
										UNWAVE UNITS</TH>
									<TH align="center" width="70">
										ITEM MERCH TYPE</TH>
									<TH align="center" width="70">
										DISTRO MERCH TYPE</TH>
									<TH align="center" width="65">
										DIVISION CODE</TH>
									<TH align="center" width="65">
									</TH>
								</TR>
								<TR>
									<TD colSpan="16">
										<HR style="COLOR: black; HEIGHT: 1px">
									</TD>
								</TR>
								<%
								'For Each _dr In _dsDetails.Tables(2).Rows
								'	code_desc = _dr("code_desc")
								'Next
								
								FOR EACH _dr in _dsDetails.Tables(0).Rows
							allocAdjustUrl = ""							
							duplicateFlag = IIF (prevSku = _dr("SKU_NUM"),TRUE,FALSE)
							 
							IF duplicateFlag THEN
								unitsShpd = """"
								unitsRcvd = """"
								rcptVar = """"	
								unitsOrdered = 	""""							
							ELSE
								unitsOrdered = _dr("UNITS_ORDERED")
								unitsShpd = _dr("UNITS_SHPD")
								unitsRcvd = _dr("UNITS_RCVD")
								totUnitsOrdered += unitsOrdered
								totShpd += _dr("UNITS_SHPD")
								totRcvd += _dr("UNITS_RCVD")
								rcptVar = _dr("UNITS_RCVD")-_dr("UNITS_SHPD")								
								IF _dr("UNITS_SHPD") > 0 THEN
									rcptVarPct =  FormatNumber(rcptVar/_dr("UNITS_SHPD") * 100,0) & "%"
								ELSE
									'rcptVarPct = IIF(rcptVar = 0, "0.00%","")
									rcptVarPct = ""
								END IF
							END IF
							prevSku = _dr("SKU_NUM")							
							totAlloc += _dr("ALLOC_UNITS")
							totWave += _dr("WAVE_UNITS")
								
							unitsBs = 0	
							unitAlloc = 0
							'get backstock										
							unitsBs = 	GetBackStockUnits(_dr("SKU_NUM"),_dr("DISTRO_NBR"))
							unitAlloc = _dr("ALLOC_UNITS") + unitsBs
							'allocVar = (_dr("ALLOC_UNITS") + unitsBs) - _dr("UNITS_RCVD")
							allocVar = unitAlloc - _dr("UNITS_RCVD")
							IF _dr("UNITS_RCVD") > 0 AndAlso unitAlloc > 0 AndAlso allocVar <> 0 THEN
								allocVarPct = 0								
								adjustQty = _dr("UNITS_RCVD") - unitAlloc
								allocVarPct = (adjustQty / unitAlloc) * 100
								'flLogFile.WriteItem("adjustQty = " & adjustQty & " , unitAlloc = " & unitAlloc)
								'IF _dr("WAVE_UNITS") = 0  THEN	
								
								'kjz
								'Response.write("<BR>Unit RCVD = " & _dr("UNITS_RCVD") &  ", unitAlloc = " & unitAlloc & ", unitsBs = " & unitsBs  & ", adjustQty = " & adjustQty & ", UOM_QTY = " & _dr("UOM_QTY") & ", allocAdjustLimit = " & allocAdjustLimit & ", allocVarPct = " & allocVarPct )
								
								'Response.write("code_desc = " & code_desc & " ---- " & String.Compare(code_desc,"End {Wave} Completed"))
								allocAdjustLimit = IIF(allocAdjustLimit < 0,allocAdjustLimit * -1,allocAdjustLimit)							
								'IF code_desc <> "Ship {Wave} Completed" THEN
									IF unitAlloc > 0 AndAlso allocVarPct >= allocAdjustLimit * -1 AndAlso allocVarPct <= allocAdjustLimit THEN
										'5/19/06 - Sri Bajjuri - Show the "Alloc Adjust" link only when adjustQty is more than UOM_QTY
										IF (adjustQty < 0) OrElse (adjustQty  >= _dr("UOM_QTY")) THEN
								                    allocAdjustUrl = "<a href=""javascript:fnAllocAdjust('" & _dr("DISTRO_NBR") & "|" & _dr("SKU_ID") & "|" & _dr("ALLOC_UNITS") & "|" & adjustQty & "|" & unitsBs & "')"" onmouseover=""window.status='Wave Tool';return true"" onmouseout=""window.status='Wave Tool';return true"">Adjust Alloc</a>"
										END IF
									ELSE
										IF unitAlloc > 0 AndAlso Math.Abs(allocVarPct) > allocAdjustLimit THEN
											'09-FEB-2010 JO Per Production Support request
											'IF adjustQty < 0 THEN
												Dim skuNum As String() = _dr("SKU_NUM").Split("-")
												itemNbr = skuNum(0)
								                    allocAdjustUrl = "<a href=""javascript:fnAllocAdjustWL('" & _dr("DISTRO_NBR") & "|" & _dr("SKU_ID") & "|" & _dr("UNITS_RCVD") & "|" & itemNbr & "|" & unitsBs & "')"" onmouseover=""window.status='Wave Tool';return true"" onmouseout=""window.status='Wave Tool';return true"">(> 5%)Adjust</a>"
											'09-FEB-2010 JO Per Production Support request
											'END IF
										END IF
									END IF
								'END IF
								'END IF
							END IF		
							totAllocVar += allocVar
							totBs += unitsBs						
							'totRcptVar = (totShpd-totRcvd) & "/" & FormatNumber((totShpd-totRcvd)/totShpd *100,2) & "%"
							totRcptVar = totRcvd-totShpd
							IF totShpd > 0 THEN
								totRcptVarPct = FormatNumber(totRcptVar/totShpd *100,0) & "%"
							ELSE								
								totRcptVarPct = ""
								'totRcptVarPct = IIF(totShpd > 0, "100.00%","0.00%")
							END IF
							
							
						%>
								<TR>
									<TD><%=_dr("DISTRO_NBR")%></TD>
									<TD><%=_dr("SKU_NUM")%></TD> <!--
									<TD><%'=allocVarPct%></TD>
									-->
									<TD align="center"><%=unitsOrdered%></TD>
									<TD align="center"><%=unitsShpd%></TD>
									<TD align="center"><%=unitsRcvd%></TD>
									<TD align="center"><%=_dr("ALLOC_UNITS")%></TD>
									<TD align="center"><%=unitsBs%></TD>
									<TD align="center"><%=rcptVar%></TD>
									<TD align="center"><%=rcptVarPct%></TD>
									<TD align="center"><%=allocVar%></TD>
									<TD align="center"><%=_dr("WAVE_UNITS")%></TD>
									<TD align="center"><%=_dr("ALLOC_UNITS") - _dr("WAVE_UNITS")%></TD>
									<TD align="center"><%=_dr("ITEM_MERCH_TYPE")%></TD>
									<TD align="center"><%=_dr("DISTRO_MERCH_TYPE")%></TD>
									<TD align="center"><%=_dr("COLOR_DESC")%></TD>
									<TD align="center"><%=allocAdjustUrl%></TD>
								</TR>
								<%NEXT%>
								<TR>
									<TD colSpan="16">
										<HR style="COLOR: black; HEIGHT: 1px">
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD></TD> <!--TD></TD-->
									<TD align="center"><%=totUnitsOrdered%></TD>
									<TD align="center"><%=totShpd%></TD>
									<TD align="center"><%=totRcvd%></TD>
									<TD align="center"><%=totAlloc%></TD>
									<TD align="center"><%=totBs%></TD>
									<TD align="center"><%=totRcptVar%></TD>
									<TD align="center"><%=totRcptVarPct%></TD>
									<TD align="center"><%=totAllocVar%></TD>
									<TD align="center"><%=totWave%></TD>
									<TD align="center"><%=totAlloc - totWave%></TD>
									<TD align="center"></TD>
									<TD align="center"></TD>
									<TD align="center"></TD>
									<TD align="center"></TD>
								</TR>
								<TR>
									<TD colSpan="16">
										<HR style="COLOR: black; HEIGHT: 1px">
									</TD>
								</TR>
							</TABLE>
							<%END IF
							CATCH EX as EXCEPTION
							
							END TRY
							%>
						</TD>
					</TR>
					<TR>
						<TD><BR>
						</TD>
					</TR>
					<TR>
						<TD vAlign="top"><!--div id="Div3" style="display:inline"-->
							<TABLE cellSpacing="0" cellPadding="0" width="800" border="0">
								<TR>
									<TD vAlign="top">
										<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="390" border="0">
											<TR>
												<TD width="100"><STRONG>DISTRO SUMMARY:</STRONG></TD>
												<TD width="100"></TD>
												<TD width="70"></TD>
												<TD></TD>
											</TR>
											<TR height="3">
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:DataGrid id="dgDistroSummary" Width="95%" Runat="server" CssClass="DatagridSm" EnableViewState="False" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="BC333333sm" ItemStyle-CssClass="BC111111sm" HeaderStyle-CssClass="DATAGRID_HeaderSm" CellPadding="1" GridLines="None" ShowFooter="True">
														<Columns>
															<asp:BoundColumn DataField="DISTRO_NBR" HeaderText="Distro #" HeaderStyle-Width="70px"></asp:BoundColumn>
															<asp:BoundColumn DataField="CODE_DESC" HeaderText="Distro Status" FooterText="Total" HeaderStyle-Width="150px"></asp:BoundColumn>
															<asp:BoundColumn DataField="CNT" HeaderText="Count" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
															<asp:BoundColumn DataField="ALLOC_UNITS" HeaderText="Alloc Units" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
														</Columns>
													</asp:DataGrid></TD>
											</TR>
											<TR height="10">
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD width="100"><STRONG>PREVIOUS WAVE?</STRONG></TD>
												<TD width="100">
													<asp:Label id="lblPrevWave" Runat="server" CssClass="cellvalueleft"></asp:Label></TD>
												<TD width="70"></TD>
												<TD></TD>
											</TR>
											<TR height="3">
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:DataGrid id="dgPrevWave" Width="95%" Runat="server" CssClass="DatagridSm" EnableViewState="False" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="BC333333sm" ItemStyle-CssClass="BC111111sm" HeaderStyle-CssClass="DATAGRID_HeaderSm" CellPadding="1" GridLines="None">
														<Columns>
															<asp:BoundColumn DataField="ship_wave_nbr" HeaderText="Wave #" FooterText="Total"></asp:BoundColumn>
															<asp:BoundColumn DataField="wave_desc" HeaderText="Wave Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
															<asp:BoundColumn DataField="code_desc" HeaderText="Wave Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
														</Columns>
													</asp:DataGrid></TD>
											</TR>
											<TR height="10">
												<TD colSpan="4"><!--<HR style="COLOR: black; HEIGHT: 1px"--></TD>
											</TR>
											<TR>
												<TD width="100"><STRONG>RESERVE LOC TYPE:</STRONG></TD>
												<TD align="right" width="100"></TD>
												<TD align="right" width="70"></TD>
												<TD></TD>
											</TR>
											<TR height="3">
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:DataGrid id="dgResvLocType" Width="95%" Runat="server" CssClass="DatagridSm" EnableViewState="False" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="BC333333sm" ItemStyle-CssClass="BC111111sm" HeaderStyle-CssClass="DATAGRID_HeaderSm" CellPadding="1" GridLines="None" ShowFooter="True">
														<Columns>
															<asp:BoundColumn DataField="RESV_LOC_TYPE" HeaderStyle-Width="150px" HeaderText="Location Type"></asp:BoundColumn>
															<asp:BoundColumn DataField="Cnt" HeaderStyle-Width="40px" HeaderText="Count" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
															<asp:TemplateColumn></asp:TemplateColumn>
														</Columns>
													</asp:DataGrid></TD>
											</TR>
											<TR height="10">
												<TD colSpan="4"><!--<HR style="COLOR: black; HEIGHT: 1px"--></TD>
											</TR>
											<TR>
												<TD colSpan="4"><STRONG>MULTIPLE SKU CASE?</STRONG> &nbsp;&nbsp;
													<asp:Label id="lblMultiSkuCase" Runat="server" CssClass="cellvalueleft" EnableViewState="False"></asp:Label></TD>
											</TR>
											<TR height="10">
												<TD colSpan="4"><!--<HR style="COLOR: black; HEIGHT: 1px"--></TD>
											</TR>
										</TABLE>
									</TD>
									<TD width="10"></TD>
									<TD vAlign="top">
										<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="390" border="0">
											<TR>
												<TD width="100"><STRONG>CASE SUMMARY:</STRONG></TD>
												<TD width="100"></TD>
												<TD width="70"></TD>
												<TD></TD>
											</TR>
											<TR height="3">
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:DataGrid id="dgCaseSummary" Width="95%" Runat="server" CssClass="DatagridSm" EnableViewState="False" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="BC333333sm" ItemStyle-CssClass="BC111111sm" HeaderStyle-CssClass="DATAGRID_HeaderSm" CellPadding="1" GridLines="None" ShowFooter="True">
														<Columns>
															<asp:BoundColumn DataField="CODE_DESC" HeaderStyle-Width="150px" HeaderText="Case Status"></asp:BoundColumn>
															<asp:BoundColumn DataField="AREA" HeaderStyle-Width="100px" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
															<asp:BoundColumn DataField="Cnt" HeaderText="Count" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
														</Columns>
													</asp:DataGrid></TD>
											</TR>
											<TR height="10">
												<TD colSpan="4"></TD>
											</TR> <!--tr>
										<td colspan="4"><hr style="COLOR: black;HEIGHT: 1px">
										</td>
									</tr-->
											<TR>
												<TD width="100"><STRONG>CASE LOCK?</STRONG>&nbsp;&nbsp;
													<asp:Label id="lblCaseLock" Runat="server" CssClass="cellvalueleft" EnableViewState="False"></asp:Label></TD>
												<TD width="100"></TD>
												<TD width="70"></TD>
												<TD></TD>
											</TR>
											<TR height="3">
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:DataGrid id="dgCaseLock" Width="95%" Runat="server" CssClass="DatagridSm" EnableViewState="False" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="BC333333sm" ItemStyle-CssClass="BC111111sm" HeaderStyle-CssClass="DATAGRID_HeaderSm" CellPadding="1" GridLines="None" ShowFooter="True">
														<Columns>
															<asp:BoundColumn DataField="INVN_LOCK_CODE" HeaderStyle-Width="150px" HeaderText="Lock Code"></asp:BoundColumn>
															<asp:BoundColumn DataField="Cnt" HeaderStyle-Width="40px" HeaderText="Count" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
															<asp:TemplateColumn></asp:TemplateColumn>
														</Columns>
													</asp:DataGrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</asp:panel></table>
		</form>
	</body>
</HTML>
