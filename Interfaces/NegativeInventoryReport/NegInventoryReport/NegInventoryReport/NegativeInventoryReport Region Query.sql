use NegativeInventory
declare @FromDate datetime = '5/4/2014', @ToDate datetime = '5/10/2014'

select 
	Region,
	sum(Ttl_U_EOP) as Ttl_U_EOP,
	sum(Ttl_C_EOP) as Ttl_C_EOP,
	sum(Neg_U_EOP) as Neg_U_EOP,
	sum(Neg_C_EOP) as Neg_C_EOP,
	case sum(Ttl_U_EOP) when 0 then 0 else sum(Neg_U_EOP)/sum(Ttl_U_EOP) end as Neg_U_EOP_Pcnt,
	case sum(Ttl_C_EOP) when 0 then 0 else sum(Neg_C_EOP)/sum(Ttl_C_EOP) end as Neg_C_EOP_Pcnt,
	dense_rank() over 
		(order by case sum(Ttl_U_EOP) when 0 then 0 else sum(Neg_U_EOP)/sum(Ttl_U_EOP) end) as RankNegU_EOP_Pcnt,
	dense_rank() over 
		(order by case sum(Ttl_C_EOP) when 0 then 0 else sum(Neg_C_EOP)/sum(Ttl_C_EOP) end) as RankNegC_EOP_Pcnt,
	sum(Txfr_OUT_U) as Txfr_OUT_U,
	sum(Txfr_OUT_C) as Txfr_OUT_C,
	case sum(Ttl_U_EOP) when 0 then 0 else sum(Txfr_OUT_U)/sum(Ttl_U_EOP) end as Txfr_OUT_U_Pcnt,
	case sum(Ttl_C_EOP) when 0 then 0 else sum(Txfr_OUT_C)/sum(Ttl_C_EOP) end as Txfr_OUT_C_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_U_EOP) when 0 then 0 else sum(Txfr_OUT_U)/sum(Ttl_U_EOP) end) as RankTxfrOUT_U_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_C_EOP) when 0 then 0 else sum(Txfr_OUT_C)/sum(Ttl_C_EOP) end) as RankTxfrOUT_C_Pcnt,
	sum(AllInvAdj_U) as AllInvAdj_U,
	sum(AllInvAdj_C) as AllInvAdj_C,
	case sum(Ttl_U_EOP) when 0 then 0 else sum(AllInvAdj_U)/sum(Ttl_U_EOP) end as AllInvAdj_U_Pcnt,
	case sum(Ttl_C_EOP) when 0 then 0 else sum(AllInvAdj_C)/sum(Ttl_C_EOP) end as AllInvAdj_C_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_U_EOP) when 0 then 0 else sum(AllInvAdj_U)/sum(Ttl_U_EOP) end) as RankAllInvAdj_U_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_C_EOP) when 0 then 0 else sum(AllInvAdj_C)/sum(Ttl_C_EOP) end) as RankAllInvAdj_C_Pcnt,
	sum(MOS_U) as MOS_U,
	sum(MOS_C) as MOS_C,
	case sum(Ttl_U_EOP) when 0 then 0 else sum(MOS_U)/sum(Ttl_U_EOP) end as MOS_U_Pcnt,
	case sum(Ttl_C_EOP) when 0 then 0 else sum(MOS_C)/sum(Ttl_C_EOP) end as MOS_C_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_U_EOP) when 0 then 0 else sum(MOS_U)/sum(Ttl_U_EOP) end) as RankMOS_U_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_C_EOP) when 0 then 0 else sum(MOS_C)/sum(Ttl_C_EOP) end) as RankMOS_C_Pcnt,
	sum(Idntcl_Item_Txfr) as Idntcl_Item_Txfr,
	sum(Dummy_SKU_Txfr) as Dummy_SKU_Txfr,
	sum(Ttl_Txfr) as Ttl_Txfr,
	case sum(Ttl_Txfr) when 0 then 0 else sum(Idntcl_Item_Txfr)/sum(Ttl_Txfr) end as Idntcl_Item_Txfr_Pcnt,
	case sum(Ttl_Txfr) when 0 then 0 else sum(Dummy_SKU_Txfr)/sum(Ttl_Txfr) end as Dummy_SKU_Txfr_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_Txfr) when 0 then 0 else sum(Idntcl_Item_Txfr)/sum(Ttl_Txfr) end) as RankIdntcl_Item_Txfr_Pcnt,
	dense_rank() over
		(order by case sum(Ttl_Txfr) when 0 then 0 else sum(Dummy_SKU_Txfr)/sum(Ttl_Txfr) end) as RankDummy_SKU_Txfr_Pcnt
from NEG_INV_FINAL
where V_DATE BETWEEN @FromDate AND @ToDate
group by Region
order by Region