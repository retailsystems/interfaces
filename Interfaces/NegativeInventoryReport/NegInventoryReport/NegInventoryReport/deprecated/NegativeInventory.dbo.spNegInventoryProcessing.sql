USE [NegativeInventory]
GO

/****** Object:  StoredProcedure [dbo].[spNegInventoryProcessing]    Script Date: 7/30/2014 4:44:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--/******************************************************************************************
--/* PROCEDURE NAME		:  spNegInventoryProcessing
--/* DATE CREATED		:  06/17/2014
--/* PROGRAMMER			:  Karl J. Zeutzius, Hottopic
--/*
--/* INPUT DATABASE		:  ORMS
--/*
--/* OUTPUT DATABASE	:  CASQLPRD1
--/*
--/* SSIS Package  		:  NegativeInventory
--/*
--/* PROCEDURE SUMMARY	:  This package is called by an SSIS package to build negative inventory
--/*						data for Region, District, and Store.  It inserts results into
--/*						table: NEG_INV_FINAL for final reporting purposes
--/*
--/****************************************************************************************
--/* MODIFICATION HISTORY:	
--/*08/04/2014	Apurva Parikh Changed all case statements to return 0 for 0 denominator
--/*
--/****************************************************************************************
-- drop proc spNegInventoryProcessing
-- grant all on spNegInventoryProcessing to public 
-- EXEC spNegInventoryProcessing 
	ALTER PROC [dbo].[spNegInventoryProcessing]
	AS
	BEGIN
	
	-- Calculate % cols and rankings at store level
	SELECT t1.V_DATE,
			t2.StoreNum,
			t2.Region,
			t2.District,
			t2.StoreName,
			t1.TTL_U_EOP,
			t1.TTL_C_EOP,
			t1.NEG_U_EOP,
			t1.NEG_C_EOP,
			CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.NEG_U_EOP/t1.TTL_U_EOP END as 'Neg_U_EOP_Pcnt',
			CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.NEG_C_EOP/t1.TTL_C_EOP END as 'Neg_C_EOP_Pcnt',
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.NEG_U_EOP/t1.TTL_U_EOP END) 
			as 'RankNegU_EOP_Pcnt',		
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.NEG_C_EOP/t1.TTL_C_EOP END) 
			as 'RankNegC_EOP_Pcnt',
			
			t1.TRANSFER_OUT_U,
			t1.TRANSFER_OUT_C,
			
			CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.TRANSFER_OUT_U/t1.TTL_U_EOP END as 'Txfr_OUT_U_Pcnt',
			CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.TRANSFER_OUT_C/t1.TTL_C_EOP END as 'Txfr_OUT_C_Pcnt',			
			
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.TRANSFER_OUT_U/t1.TTL_U_EOP END) 
			as 'RankTxfrOUT_U_Pcnt',		
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.TRANSFER_OUT_C/t1.TTL_C_EOP END)
			as 'RankTxfrOUT_C_Pcnt',
			
			t1.ALL_INVENTORY_ADJ_U,
			t1.ALL_INVENTORY_ADJ_C,
			
			CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.ALL_INVENTORY_ADJ_U/t1.TTL_U_EOP END as 'AllInvAdj_U_Pcnt',
			CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.ALL_INVENTORY_ADJ_C/t1.TTL_C_EOP END as 'AllInvAdj_C_Pcnt',			
			
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.ALL_INVENTORY_ADJ_U/t1.TTL_U_EOP END) 
			as 'RankAllInvAdj_U_Pcnt',		
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.ALL_INVENTORY_ADJ_C/t1.TTL_C_EOP END)
			as 'RankAllInvAdj_C_Pcnt',
			
			t1.MOS_U,
			t1.MOS_C,
			
			CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.MOS_U/t1.TTL_U_EOP END as 'MOS_U_Pcnt',
			CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.MOS_C/t1.TTL_C_EOP END as 'MOS_C_Pcnt',			
			
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_U_EOP WHEN 0 THEN 0 ELSE t1.MOS_U/t1.TTL_U_EOP END)
			as 'RankMOS_U_Pcnt',		
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_C_EOP WHEN 0 THEN 0 ELSE t1.MOS_C/t1.TTL_C_EOP END)
			as 'RankMOS_C_Pcnt',

			t1.IDENTICAL_ITEM_TRANS,
			t1.DUMMY_SKU_TRANS,
			
			CASE t1.TTL_TRANS WHEN 0 THEN 0 ELSE t1.IDENTICAL_ITEM_TRANS/t1.TTL_TRANS END as 'Idntcl_Item_Txfr_Pcnt',
			CASE t1.TTL_TRANS WHEN 0 THEN 0 ELSE t1.DUMMY_SKU_TRANS/t1.TTL_TRANS END as 'Dummy_SKU_Txfr_Pcnt',
			
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_TRANS WHEN 0 THEN 0 ELSE t1.IDENTICAL_ITEM_TRANS/t1.TTL_TRANS END)
			as 'RankIdntcl_Item_Txfr_Pcnt',		
			DENSE_RANK() OVER
						(PARTITION BY t2.Region,t2.District
						 Order by CASE t1.TTL_TRANS WHEN 0 THEN 0 ELSE t1.DUMMY_SKU_TRANS/t1.TTL_TRANS END)
			as 'RankDummy_SKU_Txfr_Pcnt',			
			t1.TTL_TRANS,
			t1.INSERT_DATE
	INTO #STORE_LEVEL
	FROM dbo.NEG_INV_STAGING t1,
		 dbo.STORE t2
	WHERE t2.StoreNum = t1.STORE;

	
	-- *** Calculate rankings for Districts ***
	WITH DIST_RANK_CTE 
	(	Region,
		District,
		SUM_TTL_U_EOP,
		SUM_TTL_C_EOP,
		SUM_NEG_U_EOP,
		SUM_NEG_C_EOP,
		SUM_TRANSFER_OUT_U,
		SUM_TRANSFER_OUT_C,
		SUM_ALL_INVENTORY_ADJ_U,
		SUM_ALL_INVENTORY_ADJ_C,		
		SUM_MOS_U,
		SUM_MOS_C,		
		SUM_IDENTICAL_ITEM_TRANS,
		SUM_DUMMY_SKU_TRANS,
		SUM_TTL_TRANS
	 ) 
	AS
	( 
	  SELECT	t2.Region,
				t2.District,
				SUM(t1.TTL_U_EOP) as SUM_TTL_U_EOP,
				SUM(t1.TTL_C_EOP) as SUM_TTL_C_EOP,
				
				SUM(t1.NEG_U_EOP) as SUM_NEG_U_EOP,
				SUM(t1.NEG_C_EOP) as SUM_NEG_C_EOP,
				
				SUM(t1.TRANSFER_OUT_U) as SUM_TRANSFER_OUT_U,
				SUM(t1.TRANSFER_OUT_C) as SUM_TRANSFER_OUT_C,

				SUM(t1.ALL_INVENTORY_ADJ_U) as SUM_ALL_INVENTORY_ADJ_U,
				SUM(t1.ALL_INVENTORY_ADJ_C) as SUM_ALL_INVENTORY_ADJ_C,
				
				SUM(t1.MOS_U) as SUM_MOS_U,
				SUM(t1.MOS_C) as SUM_MOS_C,
				
				SUM(t1.IDENTICAL_ITEM_TRANS) as SUM_IDENTICAL_ITEM_TRANS,
				SUM(t1.DUMMY_SKU_TRANS) as SUM_DUMMY_SKU_TRANS,
				SUM(t1.TTL_TRANS)		as SUM_TTL_TRANS
		FROM dbo.NEG_INV_STAGING t1,
			 dbo.STORE t2
		WHERE t2.StoreNum = t1.STORE
		GROUP BY t2.Region,
				 t2.District
	  )		
		SELECT	t3.Region,
				t3.District,
				t3.SUM_TTL_U_EOP,
				t3.SUM_TTL_C_EOP,
				t3.SUM_NEG_U_EOP,
				t3.SUM_NEG_C_EOP,
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_U_EOP/t3.SUM_TTL_U_EOP END as 'Dst_Neg_U_EOP_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_C_EOP/t3.SUM_TTL_C_EOP END as 'Dst_Neg_C_EOP_Pcnt',
				
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_U_EOP/t3.SUM_TTL_U_EOP END)
				as 'DistRankNegU_EOP_Pcnt',		
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_C_EOP/t3.SUM_TTL_C_EOP END)
				as 'DistRankNegC_EOP_Pcnt',
				
				t3.SUM_TRANSFER_OUT_U,
				t3.SUM_TRANSFER_OUT_C,
				
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_U/t3.SUM_TTL_U_EOP END as 'Dst_Txfr_OUT_U_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_C/t3.SUM_TTL_C_EOP END as 'Dst_Txfr_OUT_C_Pcnt',			
				
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_U/t3.SUM_TTL_U_EOP END)
				as 'DistRankTxfrOUT_U_Pcnt',
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_C/t3.SUM_TTL_C_EOP END)
				as 'DistRankTxfrOUT_C_Pcnt',
				
				t3.SUM_ALL_INVENTORY_ADJ_U,
				t3.SUM_ALL_INVENTORY_ADJ_C,
				
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_U/t3.SUM_TTL_U_EOP END as 'Dst_AllInvAdj_U_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_C/t3.SUM_TTL_C_EOP END as 'Dst_AllInvAdj_C_Pcnt',			
				
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_U/t3.SUM_TTL_U_EOP END)
				as 'DistRankAllInvAdj_U_Pcnt',
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_C/t3.SUM_TTL_C_EOP END)
				as 'DistRankAllInvAdj_C_Pcnt',
				
				t3.SUM_MOS_U,
				t3.SUM_MOS_C,
				
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_U/t3.SUM_TTL_U_EOP END as 'Dst_MOS_U_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_C/t3.SUM_TTL_C_EOP END as 'Dst_MOS_C_Pcnt',
				
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_U/t3.SUM_TTL_U_EOP END)
				as 'DistRankMOS_U_Pcnt',
				DENSE_RANK() OVER
							(PARTITION BY t3.Region
							 Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_C/t3.SUM_TTL_C_EOP END)
				as 'DistRankMOS_C_Pcnt',
				
				t3.SUM_IDENTICAL_ITEM_TRANS,
				t3.SUM_DUMMY_SKU_TRANS,
				
				CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_IDENTICAL_ITEM_TRANS/t3.SUM_TTL_TRANS END as 'Dst_Idntcl_Item_Txfr_Pcnt',
				CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_DUMMY_SKU_TRANS/t3.SUM_TTL_TRANS END as 'Dst_Dummy_SKU_Txfr_Pcnt',			
				
				DENSE_RANK() OVER
							(PARTITION BY t3.Region 
							 Order by CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_IDENTICAL_ITEM_TRANS/t3.SUM_TTL_TRANS END)
				as 'DistRankIdntcl_Item_Txfr_Pcnt',		
				DENSE_RANK() OVER
							(PARTITION BY t3.Region 
							 Order by CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_DUMMY_SKU_TRANS/t3.SUM_TTL_TRANS END)
				as 'DistRankDummy_SKU_Txfr_Pcnt',
				SUM_TTL_TRANS
		INTO #DIST_LEVEL
		FROM DIST_RANK_CTE t3;
		

	-- *** Calculate rankings for Regions ***
	WITH RGN_RANK_CTE 
	(	Region,
--		District,
		SUM_TTL_U_EOP,
		SUM_TTL_C_EOP,
		SUM_NEG_U_EOP,
		SUM_NEG_C_EOP,
		SUM_TRANSFER_OUT_U,
		SUM_TRANSFER_OUT_C,
		SUM_ALL_INVENTORY_ADJ_U,
		SUM_ALL_INVENTORY_ADJ_C,		
		SUM_MOS_U,
		SUM_MOS_C,		
		SUM_IDENTICAL_ITEM_TRANS,
		SUM_DUMMY_SKU_TRANS,
		SUM_TTL_TRANS
	 ) 
	AS
	( 
	  SELECT	t1.Region,
--				t1.District,
				SUM(t1.SUM_TTL_U_EOP) as SUM_TTL_U_EOP,
				SUM(t1.SUM_TTL_C_EOP) as SUM_TTL_C_EOP,
				
				SUM(t1.SUM_NEG_U_EOP) as SUM_NEG_U_EOP,
				SUM(t1.SUM_NEG_C_EOP) as SUM_NEG_C_EOP,
				
				SUM(t1.SUM_TRANSFER_OUT_U) as SUM_TRANSFER_OUT_U,
				SUM(t1.SUM_TRANSFER_OUT_C) as SUM_TRANSFER_OUT_C,

				SUM(t1.SUM_ALL_INVENTORY_ADJ_U) as SUM_ALL_INVENTORY_ADJ_U,
				SUM(t1.SUM_ALL_INVENTORY_ADJ_C) as SUM_ALL_INVENTORY_ADJ_C,
				
				SUM(t1.SUM_MOS_U) as SUM_MOS_U,
				SUM(t1.SUM_MOS_C) as SUM_MOS_C,
				
				SUM(t1.SUM_IDENTICAL_ITEM_TRANS) as SUM_IDENTICAL_ITEM_TRANS,
				SUM(t1.SUM_DUMMY_SKU_TRANS) as SUM_DUMMY_SKU_TRANS,
				SUM(t1.SUM_TTL_TRANS)		as SUM_TTL_TRANS
		FROM dbo.#DIST_LEVEL t1
		GROUP BY t1.Region--,
				 --t1.District
	  )		
		SELECT	t3.Region,
			--	t3.District,
				t3.SUM_TTL_U_EOP,
				t3.SUM_TTL_C_EOP,
				t3.SUM_NEG_U_EOP,
				t3.SUM_NEG_C_EOP,
				CASE t3.SUM_NEG_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_U_EOP/t3.SUM_TTL_U_EOP END as 'Rgn_Neg_U_EOP_Pcnt',
				CASE t3.SUM_NEG_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_C_EOP/t3.SUM_TTL_C_EOP END as 'Rgn_Neg_C_EOP_Pcnt',
				
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_NEG_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_U_EOP/t3.SUM_TTL_U_EOP END)
				as 'RgnRankNegU_EOP_Pcnt',		
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_NEG_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_NEG_C_EOP/t3.SUM_TTL_C_EOP END) 
				as 'RgnRankNegC_EOP_Pcnt',
				
				t3.SUM_TRANSFER_OUT_U,
				t3.SUM_TRANSFER_OUT_C,
				
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_U/t3.SUM_TTL_U_EOP END as 'Rgn_Txfr_OUT_U_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_C/t3.SUM_TTL_C_EOP END as 'Rgn_Txfr_OUT_C_Pcnt',			
				
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_U/t3.SUM_TTL_U_EOP END)
				as 'RgnRankTxfrOUT_U_Pcnt',		
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_TRANSFER_OUT_C/t3.SUM_TTL_C_EOP END)
				as 'RgnRankTxfrOUT_C_Pcnt',				
				
				t3.SUM_ALL_INVENTORY_ADJ_U,
				t3.SUM_ALL_INVENTORY_ADJ_C,
				
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_U/t3.SUM_TTL_U_EOP END as 'Rgn_AllInvAdj_U_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_C/t3.SUM_TTL_C_EOP END as 'Rgn_AllInvAdj_C_Pcnt',
				
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_U/t3.SUM_TTL_U_EOP END)
				as 'RgnRankAllInvAdj_U_Pcnt',
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_ALL_INVENTORY_ADJ_C/t3.SUM_TTL_C_EOP END)
				as 'RgnRankAllInvAdj_C_Pcnt',
				
				t3.SUM_MOS_U,
				t3.SUM_MOS_C,
				
				CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_U/t3.SUM_TTL_U_EOP END as 'Rgn_MOS_U_Pcnt',
				CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_C/t3.SUM_TTL_C_EOP END as 'Rgn_MOS_C_Pcnt',
				
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_U_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_U/t3.SUM_TTL_U_EOP END)
				as 'RgnRankMOS_U_Pcnt',
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_C_EOP WHEN 0 THEN 0 ELSE t3.SUM_MOS_C/t3.SUM_TTL_C_EOP END)
				as 'RgnRankMOS_C_Pcnt',
				
				t3.SUM_IDENTICAL_ITEM_TRANS,
				t3.SUM_DUMMY_SKU_TRANS,
				
				CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_IDENTICAL_ITEM_TRANS/t3.SUM_TTL_TRANS END as 'Rgn_Idntcl_Item_Txfr_Pcnt',
				CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_DUMMY_SKU_TRANS/t3.SUM_TTL_TRANS END as 'Rgn_Dummy_SKU_Txfr_Pcnt',			
				
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_IDENTICAL_ITEM_TRANS/t3.SUM_TTL_TRANS END)
				as 'RgnRankIdntcl_Item_Txfr_Pcnt',		
				DENSE_RANK() OVER
							(Order by CASE t3.SUM_TTL_TRANS WHEN 0 THEN 0 ELSE t3.SUM_DUMMY_SKU_TRANS/t3.SUM_TTL_TRANS END)
				as 'RgnRankDummy_SKU_Txfr_Pcnt'
		INTO #RGN_LEVEL
		FROM RGN_RANK_CTE t3;

		-- *** Now do insert to final table ***
		INSERT INTO NEG_INV_FINAL
		SELECT t1.V_DATE,
				t1.StoreNum,
				t1.Region,
				t1.District,
				t1.StoreName,
				t1.TTL_U_EOP,
				t1.TTL_C_EOP,				
				t1.NEG_U_EOP,
				t1.NEG_C_EOP,
				
				t1.Neg_U_EOP_Pcnt,
				t1.Neg_C_EOP_Pcnt,												
				t1.RankNegU_EOP_Pcnt,
				t1.RankNegC_EOP_Pcnt,				
					t2.Dst_Neg_U_EOP_Pcnt,
					t2.Dst_Neg_C_EOP_Pcnt,
				t2.DistRankNegU_EOP_Pcnt,
				t2.DistRankNegC_EOP_Pcnt,
					t3.Rgn_Neg_U_EOP_Pcnt,
					t3.Rgn_Neg_C_EOP_Pcnt,					
				t3.RgnRankNegU_EOP_Pcnt,
				t3.RgnRankNegC_EOP_Pcnt,
							
				t1.TRANSFER_OUT_U,
				t1.TRANSFER_OUT_C,
				t1.Txfr_OUT_U_Pcnt,
				t1.Txfr_OUT_C_Pcnt,
				t1.RankTxfrOUT_U_Pcnt,
				t1.RankTxfrOUT_C_Pcnt,
					t2.Dst_Txfr_OUT_U_Pcnt,
					t2.Dst_Txfr_OUT_C_Pcnt,
				t2.DistRankTxfrOUT_U_Pcnt,
				t2.DistRankTxfrOUT_C_Pcnt,
					t3.Rgn_Txfr_OUT_U_Pcnt,
					t3.Rgn_Txfr_OUT_C_Pcnt,
				t3.RgnRankTxfrOUT_U_Pcnt,
				t3.RgnRankTxfrOUT_C_Pcnt,	
						
				t1.ALL_INVENTORY_ADJ_U,
				t1.ALL_INVENTORY_ADJ_C,
				t1.AllInvAdj_U_Pcnt,
				t1.AllInvAdj_C_Pcnt,
				t1.RankAllInvAdj_U_Pcnt,
				t1.RankAllInvAdj_C_Pcnt,
					t2.Dst_AllInvAdj_U_Pcnt,
					t2.Dst_AllInvAdj_C_Pcnt,
				t2.DistRankAllInvAdj_U_Pcnt,
				t2.DistRankAllInvAdj_C_Pcnt,
					t3.Rgn_AllInvAdj_U_Pcnt,
					t3.Rgn_AllInvAdj_C_Pcnt,
				t3.RgnRankAllInvAdj_U_Pcnt,
				t3.RgnRankAllInvAdj_C_Pcnt,	
							
				t1.MOS_U,
				t1.MOS_C,
				t1.MOS_U_Pcnt,
				t1.MOS_C_Pcnt,
				t1.RankMOS_U_Pcnt,
				t1.RankMOS_C_Pcnt,
					t2.Dst_MOS_U_Pcnt,
					t2.Dst_MOS_C_Pcnt,
				t2.DistRankMOS_U_Pcnt,
				t2.DistRankMOS_C_Pcnt,				
					t3.Rgn_MOS_U_Pcnt,
					t3.Rgn_MOS_C_Pcnt,
				t3.RgnRankMOS_U_Pcnt,
				t3.RgnRankMOS_C_Pcnt,	
							
				t1.IDENTICAL_ITEM_TRANS,
				t1.DUMMY_SKU_TRANS,
				t1.Idntcl_Item_Txfr_Pcnt,
				t1.Dummy_SKU_Txfr_Pcnt,
				t1.RankIdntcl_Item_Txfr_Pcnt,
				t1.RankDummy_SKU_Txfr_Pcnt,
					t2.Dst_Idntcl_Item_Txfr_Pcnt,
					t2.Dst_Dummy_SKU_Txfr_Pcnt,
				t2.DistRankIdntcl_Item_Txfr_Pcnt,
				t2.DistRankDummy_SKU_Txfr_Pcnt,
					t3.Rgn_Idntcl_Item_Txfr_Pcnt,
					t3.Rgn_Dummy_SKU_Txfr_Pcnt,
				t3.RgnRankIdntcl_Item_Txfr_Pcnt,
				t3.RgnRankDummy_SKU_Txfr_Pcnt,		
						
				t1.TTL_TRANS,
				t1.INSERT_DATE				
		FROM #STORE_LEVEL t1,
			 #DIST_LEVEL t2,
			 #RGN_LEVEL t3
		WHERE t1.Region = t2.Region
		AND	  t1.District = t2.District
		AND	  t1.Region = t3.Region
		AND	  (t1.Region <> ''
				OR t1.District <> '')
END
--/******************************************* END OF PROC *************************************



GO


